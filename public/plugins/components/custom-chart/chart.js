
$(document).ready(function() {
    "use strict";

    if($('.dropify')[0]){
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        });
    }

    if($("#myTable")[0]){
        $(function() {

            let table = $('#myTable').DataTable({
                "language": {
                    "sProcessing": "En cours...",
                    "sZeroRecords": "Aucune occurence trouvé",
                    "sLoadingRecords": "Chargement...",
                    "sSearch":"Rechercher",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sLast": "Dernier",
                        "sNext": "Suivant",
                        "sPrevious": "Précédent"
                    },
                },
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
            });
            let table2 = $('#dateTable').DataTable({
                "language": {
                    "sSearch":"Rechercher",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sLast": "Dernier",
                        "sNext": "Suivant",
                        "sPrevious": "Précédent"
                    },
                },
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
            });
        });
    }

    if($(".qcm_chart").length>0){
        $('.qcm_chart').each(function(){
            let theId = $(this).data('id');
            let label = $(this).data('label');
            let percent = $(this).data('percent');
            let data = [];
            let c = ["#c20909", "#c28f09", "#6ac209", "#0997c2", "#5209c2", "#c20954", "#dff100", "#7f6fb1", "#ffdbb3"];
            let color = [];
            for (let i = 0; i < label.length; i++) {
                data.push({label: label[i], value: percent[i]});
                color.push(c[i])
            }
            Morris.Donut({
                element: theId,
                data: data,
                resize: true,
                colors: color
            });
        });

    }


    function createchart(id) {

        let el = document.getElementById(id); // get canvas

        let options = {
            percent: el.getAttribute('data-percent') || 50,
            size: el.getAttribute('data-size') || 160,
            lineWidth: el.getAttribute('data-line') || 5,
            inText : el.getAttribute('data-text') || 'null',
            rotate: el.getAttribute('data-rotate') || 0,
            color: el.getAttribute('data-color') || '#000',
            textColor: el.getAttribute('data-text-color') || '#000'
        }

        let canvas = document.createElement('canvas');
        let h1 = document.createElement('h1');
        h1.setAttribute('class', 'per-number');
        h1.innerHTML = options.inText ;
        h1.style.color = options.textColor;
        h1.style.width = options.size+'px';
        let hei = parseInt(options.size) - 6 ;
        h1.style.lineHeight = hei+'px';

        if (typeof(G_vmlCanvasManager) !== 'undefined') {
            G_vmlCanvasManager.initElement(canvas);
        }

        let ctx = canvas.getContext('2d');
        canvas.width = canvas.height = options.size;

        el.appendChild(h1);
        el.appendChild(canvas);

        ctx.translate(options.size / 2, options.size / 2); // change center
        ctx.rotate((-1 / 2 + options.rotate / 180) * Math.PI); // rotate -90 deg

        //imd = ctx.getImageData(0, 0, 240, 240);
        let radius = (options.size - options.lineWidth) / 2;

        let drawCircle = function(color, lineWidth, percent) {
            if(percent == 0)
                return false;
            percent = Math.min(Math.max(0, percent || 1), 1);
            ctx.beginPath();
            ctx.arc(0, 0, radius, 0, Math.PI * 2 * percent, false);
            ctx.strokeStyle = color;
            ctx.lineCap = 'round'; // butt, round or square
            ctx.lineWidth = lineWidth
            ctx.stroke();
        };

        drawCircle('#e5ebec', options.lineWidth, 100 / 100);
        drawCircle(options.color, options.lineWidth, options.percent / 100);
    }
});
