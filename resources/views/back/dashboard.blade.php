@extends('back.layout.app')
@section('content')
<div class="container-fluid">
    <div class="row colorbox-group-widget">
        <div class="col-md-3 col-sm-6 info-color-box">
            <div class="white-box">
                <div class="media bg-primary">
                    <div class="media-body">
                        <h3 class="info-count">{{$countS}}<span class="pull-right"><i class="mdi mdi-clipboard-text"></i></span></h3>
                        <p class="info-text font-12">@php print strtoupper("sondages créés") @endphp</p>
                        <p class="info-ot font-15">Sondages restant<span class="label label-rounded">{{5 - $countS}}</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 info-color-box">
            <div class="white-box">
                <div class="media bg-success">
                    <div class="media-body">
                        <h3 class="info-count">68 <span class="pull-right"><i class="mdi mdi-comment-text-outline"></i></span></h3>
                        <p class="info-text font-12">Complaints</p>
                        <p class="info-ot font-15">Total Pending<span class="label label-rounded">154</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 info-color-box">
            <div class="white-box">
                <div class="media bg-danger">
                    <div class="media-body">
                        <h3 class="info-count">$9475 <span class="pull-right"><i class="mdi mdi-coin"></i></span></h3>
                        <p class="info-text font-12">Profit</p>
                        <p class="info-ot font-15">Pending<span class="label label-rounded">236</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 info-color-box">
            <div class="white-box">
                <div class="media bg-warning">
                    <div class="media-body">
                        <h3 class="info-count">$124,356 <span class="pull-right"><i class="mdi mdi-coin"></i></span></h3>
                        <p class="info-text font-12">Total Profit</p>
                        <p class="info-ot font-15">Pending<span class="label label-rounded">782</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12">
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="white-box user-table">
                <div class="row">
                    <div class="col-sm-6">
                        <h4 class="box-title"> </h4>
                    </div>
                    <div class="col-sm-6">
                        <select class="custom-select">
                            <option selected>Sort by</option>
                            <option value="1">Name</option>
                            <option value="2">Location</option>
                            <option value="3">Type</option>
                            <option value="4">Role</option>
                            <option value="5">Action</option>
                        </select>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>
                                <div class="checkbox checkbox-info">
                                    <input id="c1" type="checkbox">
                                    <label for="c1"></label>
                                </div>
                            </th>
                            <th>Name</th>
                            <th>Location</th>
                            <th>Type</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-info">
                                    <input id="c5" type="checkbox">
                                    <label for="c5"></label>
                                </div>
                            </td>
                            <td><a href="javascript:void(0);" class="text-link">Elliot Dugteren</a></td>
                            <td>San Antonio, US</td>
                            <td>Posts 34</td>
                            <td><span class="label label-warning">General</span> </td>
                            <td>
                                <select class="custom-select">
                                    <option value="1">Modulator</option>
                                    <option value="2">Admin</option>
                                    <option value="3">Staff</option>
                                    <option value="4">User</option>
                                    <option value="5">General</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-info">
                                    <input id="c6" type="checkbox">
                                    <label for="c6"></label>
                                </div>
                            </td>
                            <td><a href="javascript:void(0);" class="text-link">Sergio Milardovich</a></td>
                            <td>Jacksonville, US</td>
                            <td>Posts 31</td>
                            <td><span class="label label-primary">Partial</span> </td>
                            <td>
                                <select class="custom-select">
                                    <option value="1">Modulator</option>
                                    <option value="2">Admin</option>
                                    <option value="3">Staff</option>
                                    <option value="4">User</option>
                                    <option value="5">General</option>
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <ul class="pagination">
                    <li class="disabled"> <a href="#">1</a> </li>
                    <li class="active"> <a href="#">2</a> </li>
                    <li> <a href="#">3</a> </li>
                    <li> <a href="#">4</a> </li>
                    <li> <a href="#">5</a> </li>
                </ul>
                <a href="javascript:void(0);" class="btn btn-success pull-right m-t-10 font-20">+</a>
            </div>
        </div>
    </div>
</div>
    @endsection