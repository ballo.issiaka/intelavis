@extends('back.layout.app')
@section('content')

<div class="container-fluid">
    <!-- New survey -->
    <div class="white-box upload-widget col-md-2">
        <div class="t-a-c">
            <div class="complete-chart" id="graph1" data-percent="45" data-size="160" data-line="5" data-rotate="0" data-color="#e74a25" data-text-color="#e74a25"><h1 class="per-number" style="color: rgb(231, 74, 37);">45<span class="per-icon">%</span></h1><canvas height="160" width="160"></canvas></div>
            <p class="font-20 m-b-0">Uploading</p>
            <p class="text-primary font-semibold font-18">Myphotos_friends.png</p>
        </div>
    </div>

    <!-- New survey End-->

    <div class="white-box upload-widget col-md-2">
        <div class="t-a-c">
            <div class="complete-chart" id="graph1" data-percent="45" data-size="160" data-line="5" data-rotate="0" data-color="#e74a25" data-text-color="#e74a25"><h1 class="per-number" style="color: rgb(231, 74, 37);">45<span class="per-icon">%</span></h1><canvas height="160" width="160"></canvas></div>
            <p class="font-20 m-b-0">Uploading</p>
            <p class="text-primary font-semibold font-18">Myphotos_friends.png</p>
        </div>
    </div>
</div>

@endsection