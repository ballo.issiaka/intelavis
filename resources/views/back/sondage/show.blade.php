@extends('back.layout.app')
@section('content')

    <questions inline-template>
        <div class="container-fluid">
            <div class="m-t-20">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12">
                        <div class="">
                            <ul class="nav customtab2 nav-tabs white-box" role="tablist">
                                <li role="presentation" class="active"><a href="#question6" aria-controls="questions"
                                                                          role="tab"
                                                                          data-toggle="tab" aria-expanded="false"><span
                                                class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Questions</span></a>
                                </li>
                                <li role="presentation" class=""><a href="#messages6" aria-controls="messages" role="tab"
                                                                    data-toggle="tab" aria-expanded="false"><span
                                                class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">Modifier</span></a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="question6">
                                    @include('back.sondage.questions')
                                </div>
                                <div role="tabpanel " class="tab-pane fade" id="messages6">
                                    @include('back.sondage.edit')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </questions>

@endsection
