<div class="row">
    <div class="col-md-12 row">
        <div class="col-md-4 row" xmlns:v-on="http://www.w3.org/1999/xhtml" xmlns:v-on="http://www.w3.org/1999/xhtml"
             xmlns:v-on="http://www.w3.org/1999/xhtml" xmlns:v-on="http://www.w3.org/1999/xhtml">
            @if(isset($sondage))
                @include('back.sondage.common.nav_question')
            @endif

        </div>
        <form class="form-horizontal" method="POST" action="{{ route('questions.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="col-md-8 pull-right row white-box">
                <div class="form-group col-sm-12 row">
                    <label class="col-md-12">Entrer la question : <span class="help"> e.x. "aimez-vous les bonobos?"</span></label>
                    <div class="col-md-12">
                        <input type="text" class="form-control" value="" name="question_lbl" required>
                        <input type="hidden" class="form-control" value="{{$sondage->id}}" name="sondage_id">
                    </div>
                </div>
                <div class="form-group col-sm-12 row text text-warning">
                    <div class="col-md-12"><input type="checkbox" id="mandatory" name="mandatory" value="1">
                        <label for="mandatory" class="">Rendre la question obligatoire<sup>(*)</sup></label></div>
                </div>
                @include('back.sondage.common.option_nav')
                <div class="col-md-12 m-10 p-20 shadow">
                    <button type="submit" class="btn btn-success btn-lg form-control ">Cliquez ici pour créer la question</button>
                </div>
            </div>

        </form>

        <div class="clearfix "></div>
    </div>
</div>

@section('scripts')

    <script>

    </script>

    @endsection