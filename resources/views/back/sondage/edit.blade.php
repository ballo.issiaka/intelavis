

    <div>
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Modifier le sondage</h3>
                    <p class="text-muted m-b-30 font-13">  </p>
                    <form class="form-horizontal" method="POST" action="{{route('sondages.update',$sondage->id)}}" enctype="multipart/form-data" >
                        @csrf
                        <div class="form-group  @if($errors->has('titre')) has-error @endif">
                            <label class="col-md-12">Titre</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" value="{{ $sondage->titre }}"  name="titre">
                                @if($errors->has('titre'))
                                    <span class="help-block"> Titre non conforme. </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group  @if($errors->has('logo')) has-error @endif">
                            <label class="col-md-12">Logo</label>
                            <div class="col-md-12">
                                <input type="file" id="input-file-now" class="dropify" name="logo" value="{{ asset('public/images/admin/sondages/'.$sondage->logo) }}" data-default-file="{{ asset('public/images/admin/sondages/'.$sondage->logo) }}"/>
                                @if($errors->has('logo'))
                                    <span class="help-block"> Image pas compatible. </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group @if($errors->has('start')) has-error @endif">
                            <label class="col-md-12">Mot de Bienvenue</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" value="{{ $sondage->start_text }}" name="start">
                                @if($errors->has('start'))
                                    <span class="help-block"> Mot de debut requis </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group @if($errors->has('end')) has-error @endif">
                            <label class="col-md-12">Mot de fin</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" value="{{ $sondage->end_text }}" name="end">
                                @if($errors->has('end'))
                                    <span class="help-block"> Mot de fin requis </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group @if($errors->has('couleur')) has-error @endif">
                            <label class="col-md-12">Choix de la couleur</label>
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <input type="color" class="form-control" value="{{ $sondage->couleur }}" name="couleur">
                                </div>
                                @if($errors->has('couleur'))
                                    <span class="help-block"> definir une couleur </span>
                                @endif
                            </div>
                        </div>
                        <!--<div class="form-group row ">
                            <label class="col-md-12">Plateforme</label>
                            <div class="col-md-8">
                                <div class="checkbox checkbox-success  col-md-2">
                                    <input id="checkbox1" type="hidden" name="mobile" value="0">
                                    <input id="checkbox1" type="checkbox" name="mobile" value="1">
                                    <label for="checkbox1"> Mobile </label>
                                </div>
                                <div class="checkbox checkbox-success col-md-2">
                                    <input id="checkbox2" type="hidden" name="web" value="0">
                                    <input id="checkbox2" type="checkbox" name="web" value="1">
                                    <label for="checkbox2"> Web </label>
                                </div>
                                <div class="checkbox checkbox-success col-md-2">
                                    <input id="checkbox3" type="hidden" name="mail" value="0">
                                    <input id="checkbox3" type="checkbox" name="mail" value="1">
                                    <label for="checkbox3"> Mail </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Sondage Anonyme</label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-info">
                                        <input type="radio" checked="checked" name="anonyme" id="radio1" value="0">
                                        <label for="radio1">Oui</label>
                                    </div>
                                </label>
                                <label class="radio-inline">
                                    <div class="radio radio-info">
                                        <input type="radio" name="anonyme" id="radio2" value="1">
                                        <label for="radio2">Non </label>
                                    </div>
                                </label>
                            </div>
                        </div>-->


                        <button type="submit" class="btn btn-success"> Modifier le sondage</button>
                        <button  class="btn btn-danger" onclick="location.href='{{route('sondages.show',$sondage)}}'"> Annuler</button>

                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- ===== Right-Sidebar-End ===== -->
    </div>



