
<div class="form-group col-sm-12 row">
    <hr>
    <label class="col-md-12">Choisir le type de réponse à cette question :</label>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">

                <input type="radio" name="choice" value="chx1" v-model="picked" id="choice_1" checked>
                <label for="choice_1" class="">QCM AVEC SMILEY </label>
            </div>
            <div class="col-md-4">
                <input type="radio" id="choice_2" name="choice" value="chx2" v-model="picked">
                <label for="choice_2" class="">QCM</label>
            </div>
            <div class="col-md-4">
                <input type="radio" id="choice_3" name="choice" value="chx3" v-model="picked">
                <label for="choice_3" class="">OUI/NON</label>
            </div>
            <div class="col-md-4">
                <input type="radio" id="choice_4" name="choice" value="chx4" v-model="picked">
                <label for="choice_4" class="">DATE</label>
            </div>
            <div class="col-md-4">
                <input type="radio" id="choice_5" name="choice" value="chx5" v-model="picked">
                <label for="choice_5" class="">TEXTE</label>
            </div>
            <div class="col-md-4">
                <input type="radio" id="choice_6" name="choice" value="chx6" v-model="picked">
                <label for="choice_6" class="">NUMERIQUE</label>
            </div>
        </div>

    </div>
</div>
<div class="col-sm-12 row shadow m-5 chx1" v-if="picked === 'chx1'">
    <h4 class="bg-red">QCM AVEC SMILEY</h4>
    <hr>
    <div class="form-group col-sm-3 row">
        <label class="col-md-12 w3-text-green w3-center"><h1>😄</h1> Excellent</label>
        <div class="col-md-12">
            <input type="text" value="excellent" class="form-control w3-center" placeholder="Excellent"
                   name="question_smiley_1">
            <input type="hidden" value="😄" class="form-control" name="path_img_1">
        </div>
    </div>
    <div class="form-group col-sm-3 row">
        <label class="col-md-12 w3-text-light-green w3-center"><h1>😊</h1> Bon</label>
        <div class="col-md-12">
            <input type="text" value="bon" class="form-control w3-center" placeholder="Bon"
                   name="question_smiley_2">
            <input type="hidden" value="😊" class="form-control" name="path_img_2">
        </div>
    </div>
    <div class="form-group col-sm-3 row">
        <label class="col-md-12 w3-text-orange w3-center"><h1>😐</h1> Moyen</label>
        <div class="col-md-12">
            <input type="text" value="moyen" class="form-control w3-center" placeholder="Moyen"
                   name="question_smiley_3">
            <input type="hidden" value="😐" class="form-control" name="path_img_3">
        </div>
    </div>
    <div class="form-group col-sm-3 row">
        <label class="col-md-12 w3-text-red w3-center"><h1>😞</h1> Insuffisant</label>
        <div class="col-md-12">
            <input type="text" value="insuffisant" class="form-control w3-center"
                   placeholder="Insuffisant"
                   name="question_smiley_4">
            <input type="hidden" value="😞" class="form-control" name="path_img_4">
        </div>
    </div>
</div>
<div class="col-sm-12 shadow m-5 row chx2" v-if="picked === 'chx2'">
    <h4>QCM</h4>
    <hr>
    <div class="col-md-12">
        <div class=" row">
            <div class="col-md-6">
                <input type="radio" value="lvl2_1" id="qcm_choice_1" v-model="lvl2_picked"
                       name="lvl2_chx">
                <label for="qcm_choice_1" class="">Texte</label>
            </div>
            <div class="col-md-6">
                <input type="radio" value="lvl2_2" id="qcm_choice_2" v-model="lvl2_picked"
                       name="lvl2_chx" checked>
                <label for="qcm_choice_2" class="">Pictogramme</label>
            </div>
        </div>
    </div>
    <div class="form-group col-sm-12 row text text-warning">
        <div class="col-md-12">
            <input type="checkbox" id="multi_choice" name="multi_choice" value="1">
            <label for="multi_choice" class="">choix multiple <sup>(*)</sup></label>
        </div>
    </div>
    <div class="col-sm-12 shadow m-5 row" v-if="lvl2_picked === 'lvl2_1'">
        <div is="lvl2_1" v-for="lvl2_1 in lvl2_1s" v-bind:inputId="lvl2_1.id">

        </div>
        <div class="col-sm-4 form-group m-t-15">
            <div class="col-md-12">
                <button class="btn btn-default btn-sm " type="button" v-on:click="addInput"><i
                            class="fa fa-plus-circle fa-2x w3-text-green"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="col-sm-12 shadowrow imgctrl " v-if="lvl2_picked === 'lvl2_2'">

        <imgctrl class="col-sm-4 form-group m-t-15 row w3-center" v-bind:idctrl="'show'+drop"
                 v-for="drop in dropifys"></imgctrl>
        <div class="col-sm-4 form-group m-t-15 row">
            <div class="center-block">
                <button type="button" class="btn btn-default btn-sm" v-on:click="addImgctrl">
                    <i class="fa fa-plus-circle fa-3x w3-text-green"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 row shadow m-5 chx3" v-if="picked === 'chx3'">
    <h4>OUI / NON</h4>
    <hr>
    <div class="col-md-12">
        <div class="row p-20">
            <div class="col-md-5 row">
                <div class="col-md-6 ">
                    <i class="w3-green fa-2x">✔</i>
                </div>
                <div class="col-md-5 p-5 col-offset-md-2">
                    <input type="text"
                           class="form-control input-lg"
                           name="yes_input"
                           value="OUI" required></div>
            </div>
            <div class="col-md-5 row col-md-offset-2">
                <div class="col-md-5 p-5">
                    <i class="w3-red fa-2x">✘</i>
                </div>
                <div class="col-md-6">
                    <input type="text"
                           class="form-control input-lg"
                           name="no_input"
                           value="NON" required></div>
            </div>
        </div>
    </div>
</div>