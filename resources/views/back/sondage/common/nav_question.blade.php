@if(count($questions) > 0)
    @foreach($questions as $key => $question)
        <div class="col-lg-12 col-sm-12 ">
            <div class="panel panel-inverse">
                <div class="panel-heading"><i class="pull-left w3-badge">{{$key + 1}}</i> <span
                            class="p-10">{{$question->titre_question}}</span>
                    <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                    </div>
                </div>
                <div class="panel-wrapper collapse" aria-expanded="false" style="">
                    <div class="panel-body row">
                        @switch ($question->type_reponse_id)
                            @case(1)
                            <div class="row">
                                <div class="col-md-12">

                                </div>
                            </div>
                            <div class="w3-row m-l-40">
                                @foreach($question->options as $option)
                                    <div class="w3-dropdown-hover ">
                                        <button class="btn btn-default">{{$option->path_img}}</button>
                                        <div class="w3-dropdown-content w3-bar-block w3-card-4">
                                            <a href="{{route('sousquestion.create',$option)}}"
                                               class="w3-bar-item w3-button">sous-questions</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @break
                            @case(2)
                            <div class="row">
                                <div class="col-md-12">

                                </div>
                            </div>
                            <div class="w3-row m-l-30">
                                @foreach($question->options as $option)
                                    <div class="w3-dropdown-hover m-5">
                                        <button class="btn btn-default">{{$option->titre_option}}</button>
                                        <div class="w3-dropdown-content w3-bar-block w3-card-4">
                                            <a href="{{route('sousquestion.create',$option)}}"
                                               class="w3-bar-item w3-button">sous-questions</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @break
                            @case(3)
                            <div class="row">
                                <div class="col-md-12">

                                </div>
                            </div>
                            <div class="w3-row m-l-30">
                                @foreach($question->options as $option)
                                    <div class="w3-dropdown-hover m-5">
                                        <button class="btn btn-default">{{$option->titre_option}}</button>
                                        <div class="w3-dropdown-content w3-bar-block w3-card-4">
                                            <a href="{{route('sousquestion.create',$option)}}"
                                               class="w3-bar-item w3-button">sous-questions</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @break
                        @endswitch
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@else
    <div class="col-md-12"> <span class="alert alert-info ">aucune question enregistré !</span></div>
@endif
