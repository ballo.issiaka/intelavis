@extends('back.layout.app')
@section('content')
    <sousquestion inline-template>
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-sm-4 col-md-4 row">
                        @include('back.sondage.common.nav_question')
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ route('sousquestion.store') }}"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-8 pull-right row shadow w3-card w3-white p-20">
                            <div class="form-group col-sm-12 row">
                                <label class="col-md-12">Entrer la question : <span class="help"> e.x. "aimez-vous les bonobos?"</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" value="" name="question_lbl" required>
                                    <input type="hidden" class="form-control" value="{{$option->question->sondage->id}}" name="sondage_id">
                                    <input type="hidden" class="form-control" value="{{$option->id}}" name="option_id">
                                </div>
                            </div>
                            <!-- include du fichier contenant les champs du formulaire de question commun aussi a sous question -->
                            @include('back.sondage.common.option_nav')
                            <div class="col-md-12 m-10 p-20 shadow">
                                <button type="submit" class="btn btn-success btn-lg form-control ">Cliquez ici pour créer la question</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </sousquestion>
@endsection