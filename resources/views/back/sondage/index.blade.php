@extends('back.layout.app')

@section('content')

    <div class="container-fluid">
        <div class="white-box text-center">
            <h1>SONDAGES</h1>
        </div>


        <div class="row  bg-transparent">
            <div class="col-md-2">
                <div class="row">
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-4">
                        <br> <br> <br>
                        <button type="button" class="btn btn-danger btn-circle btn-xl" onclick="location.href='{{route('sondages.create')}}'"><i class="fa fa-plus"></i> </button>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="row">
                    @foreach($sondages as $sondage)
                    <div class="col-md-3 ">
                        <div class="ribbon-wrapper-right-bottom bg-white">
                            <div class="ribbon ribbon-corner ribbon-info ribbon-right ribbon-bottom"><a href="#deleteSondage" data-toggle="modal" data-sondage-id="{{$sondage->id}}" data-sondage-titre="{{$sondage->titre}}"><i class="fa fa-trash fa-2x" data-toggle="modal"></i></a></div>
                            <div class="ribbon-content text-center ">
                                <a href="{{route('sondages.show',$sondage->id)}}">
                                    <img src="{{asset('images/admin/survey.png')}}" width="110" height="100">
                                </a>
                            </div>
                            <br>
                            <div class="text-center font-20">
                                {{ $sondage->titre }}
                            </div>
                            <br>
                            <div class="text-center row">
                                <div class="col-sm-offset-2 col-sm-3 btn white-box " title="@if ($sondage->mobile===1) actif @else inactif  @endif">
                                    <i class="fa fa-tablet fa-2x @if ($sondage->mobile===1) text-success  @endif" aria-hidden="true" ></i>
                                </div>
                                <div class="col-sm-3 btn white-box " title="@if ($sondage->web===1) actif @else inactif  @endif">
                                    <i class="fa fa-globe fa-2x @if ($sondage->web===1) text-success  @endif" aria-hidden="true" ></i>

                                </div>
                                <div class="col-sm-3 btn white-box" title="@if ($sondage->email===1) actif @else inactif  @endif">
                                    <i class="fa fa-envelope fa-2x @if ($sondage->email===1) text-success  @endif" aria-hidden="true"></i>
                                </div>

                            </div>
                        </div>
                        <br>
                    </div>
                     @endforeach
                </div>

            </div>


        </div>

    </div>

    <div id="deleteSondage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Supprimer</h4> </div>
                <div class="modal-body">
                    <h4>Ête vous de supprimer ce sondage <span id="sondageTitre"> </span>?</h4>
                </div>
                <div class="modal-footer">
                    <form id="userForm" action="" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-danger">Supprimer</button>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


@endsection

@section('scripts')

    <script>

        $('#deleteSondage').on('show.bs.modal', function(e) {


            var sondageId = $(e.relatedTarget).data('sondage-id');
            var sondageTitre = $(e.relatedTarget).data('sondage-titre');


            $('#userForm').attr("action", "{{ url('/sondages') }}" + "/" + sondageId);
            $('#sondageTitre').val(sondageTitre)
        });

    </script>

@endsection
