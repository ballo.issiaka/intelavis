<div class="col-md-5 row shadow p-10 m-10 " style="height: 300px; width:45%" >
    <div class="col-md-12 row m-10">
        <div class="col-md-3">
            <span class="w3-badge w3-xxlarge w3-left">Q{{1+$key}}</span>
        </div>
        <div class="col-md-9">
            <h3> {{$question['titre_question']}}</h3>
        </div>
    </div>
    <div class="col-md-12 m-t-20 row">
        @foreach($question['options'] as $option)
            <div class="m-l-10 col-md-3 row">

                <div class="w3-center m-10">

                    <div class="complete-chart" id="graphsmile{{$option['id']}}" data-percent="{{$option['stat']}}" data-size="80"
                         data-line="5"
                         data-rotate="0"
                         data-text="{{$option['path_img']}}"
                         data-color="#e74a25" data-text-color="#e74a25">
                    </div>
                    <br>
                    <div class="row w3-center">
                        <div class="col-md-12 text text-success">
                            {{round($option['stat'],2)}}%
                        </div>
                        <div class="col-md-12">
                            {{$option['titre_option']}}
                        </div>
                    </div>
                </div>

            </div>
        @endforeach
    </div>


</div>