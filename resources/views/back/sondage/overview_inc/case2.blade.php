<div class="col-md-5 row shadow p-10 m-10 " style="height: 300px;  width:45%" data-simplebar>
    <div class="col-md-12 row m-10">
        <div class="col-md-4">
            <span class="w3-badge w3-xxlarge w3-left">Q{{1+$key}}</span>
        </div>
        <div class="col-md-8">
            <h3>{{$question['titre_question']}}</h3>
        </div>
    </div>
    <div class="col-md-12">
        <div>
            <div class="qcm_chart" id='chart{{$question["id"]}}'
                 style="height: 200px;"
                 data-id='chart{{$question["id"]}}'
                 data-label='{{$question["data"]}}'
                 data-percent='{{$question["per"]}}'></div>
        </div>
    </div>

</div>
