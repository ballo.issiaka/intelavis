<div class="col-md-5 row shadow p-10 m-10 " style="height: 300px; width:45%" data-simplebar>
    <div class="col-md-12 row m-10">
        <div class="col-md-3">
            <span class="w3-badge w3-xxlarge w3-left">Q{{1+$key}}</span>
        </div>
        <div class="col-md-9">
            <h3> {{$question['titre_question']}}</h3>
        </div>
    </div>
    <div class="col-md-12 m-t-20 row">
        <div class="table-responsive">
            <table id="myTable" class="table table-striped">
                <thead>
                <tr>
                    <th>Nom & Prenoms</th>
                    <th>Email</th>
                    <th>Contact</th>
                </tr>
                </thead>
                <tbody>
                @foreach($questions_array[$key]['participants'] as $participant)
                    <tr>
                        <td>{{$participant[0]['nom']}}</td>
                        <td>{{$participant[0]['email']}}</td>
                        <td>{{$participant[0]['numero']}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


</div>