@extends('back.layout.app')
@section('content')


    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading"> With two column</div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <form action="#">
                                <div class="form-body">
                                    <h3 class="box-title">Person Info</h3>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">First Name</label>
                                                <input type="text" id="firstName" class="form-control" placeholder="John doe"> <span class="help-block"> This is inline help </span> </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group has-error">
                                                <label class="control-label">Last Name</label>
                                                <input type="text" id="lastName" class="form-control" placeholder="12n"> <span class="help-block"> This field has error. </span> </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Gender</label>
                                                <select class="form-control">
                                                    <option value="">Male</option>
                                                    <option value="">Female</option>
                                                </select> <span class="help-block"> Select your gender </span> </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Date of Birth</label>
                                                <input type="text" class="form-control" placeholder="dd/mm/yyyy"> </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Category</label>
                                                <select class="form-control" data-placeholder="Choose a Category" tabindex="1">
                                                    <option value="Category 1">Category 1</option>
                                                    <option value="Category 2">Category 2</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Membership</label>
                                                <div class="radio-list">
                                                    <label class="radio-inline p-0">
                                                        <div class="radio radio-info">
                                                            <input type="radio" name="radio" id="radio1" value="option1">
                                                            <label for="radio1">Option 1</label>
                                                        </div>
                                                    </label>
                                                    <label class="radio-inline">
                                                        <div class="radio radio-info">
                                                            <input type="radio" name="radio" id="radio2" value="option2">
                                                            <label for="radio2">Option 2 </label>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="white-box user-table">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="box-title"> </h4>
                        </div>
                        <div class="col-sm-6">
                            <select class="custom-select">
                                <option selected>Sort by</option>
                                <option value="1">Name</option>
                                <option value="2">Location</option>
                                <option value="3">Type</option>
                                <option value="4">Role</option>
                                <option value="5">Action</option>
                            </select>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>
                                    <div class="checkbox checkbox-info">
                                        <input id="c1" type="checkbox">
                                        <label for="c1"></label>
                                    </div>
                                </th>
                                <th>Name</th>
                                <th>Location</th>
                                <th>Type</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <div class="checkbox checkbox-info">
                                        <input id="c5" type="checkbox">
                                        <label for="c5"></label>
                                    </div>
                                </td>
                                <td><a href="javascript:void(0);" class="text-link">Elliot Dugteren</a></td>
                                <td>San Antonio, US</td>
                                <td>Posts 34</td>
                                <td><span class="label label-warning">General</span> </td>
                                <td>
                                    <select class="custom-select">
                                        <option value="1">Modulator</option>
                                        <option value="2">Admin</option>
                                        <option value="3">Staff</option>
                                        <option value="4">User</option>
                                        <option value="5">General</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="checkbox checkbox-info">
                                        <input id="c6" type="checkbox">
                                        <label for="c6"></label>
                                    </div>
                                </td>
                                <td><a href="javascript:void(0);" class="text-link">Sergio Milardovich</a></td>
                                <td>Jacksonville, US</td>
                                <td>Posts 31</td>
                                <td><span class="label label-primary">Partial</span> </td>
                                <td>
                                    <select class="custom-select">
                                        <option value="1">Modulator</option>
                                        <option value="2">Admin</option>
                                        <option value="3">Staff</option>
                                        <option value="4">User</option>
                                        <option value="5">General</option>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <ul class="pagination">
                        <li class="disabled"> <a href="#">1</a> </li>
                        <li class="active"> <a href="#">2</a> </li>
                        <li> <a href="#">3</a> </li>
                        <li> <a href="#">4</a> </li>
                        <li> <a href="#">5</a> </li>
                    </ul>
                    <a href="javascript:void(0);" class="btn btn-success pull-right m-t-10 font-20">+</a>
                </div>
            </div>
        </div>
    </div>

@endsection