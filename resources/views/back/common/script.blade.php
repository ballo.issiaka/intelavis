
<script src="{{asset('js/clipboard.min.js')}}"></script>


<!-- ===== jQuery ===== -->
<script src="{{asset('/plugins/components/jquery/dist/jquery.min.js')}}"></script>
<!-- ===== Bootstrap JavaScript ===== -->
<script src="{{asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- ===== Slimscroll JavaScript ===== -->
<script src="{{asset('js/jquery.slimscroll.js')}}"></script>
<!-- ===== Wave Effects JavaScript ===== -->
<script src="{{asset('js/waves.js')}}"></script>
<!-- ===== Menu Plugin JavaScript ===== -->
<script src="{{asset('js/sidebarmenu.js')}}"></script>
<!-- ===== Custom JavaScript ===== -->
<script src="{{asset('js/custom.js')}}"></script>


<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/canvasjs.min.js') }}"></script>



<!-- ===== Plugin JS =====-->
<script src="{{asset('plugins/components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('plugins/components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script>
<script src="{{asset('plugins/components/dropify/dist/js/dropify.min.js')}}"></script>
<script src="{{asset('plugins/components/sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('plugins/components/custom-chart/chart.js')}}"></script>
<script src="{{asset('plugins/components/raphael/raphael-min.js')}}"></script>
<script src="{{asset('plugins/components/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/components/morrisjs/morris.js')}}"></script>
<script src="{{asset('plugins/components/simplebar/simplebar.js')}}"></script>
<!-- icheck -->
<script src="{{asset('plugins/components/icheck/icheck.min.js')}}"></script>
<script src="{{asset('plugins/components/icheck/icheck.init.js')}}"></script>
<!-- ===== Style Switcher JS ===== -->
<script src="{{asset('plugins/components/styleswitcher/jQuery.style.switcher.js')}}"></script>


