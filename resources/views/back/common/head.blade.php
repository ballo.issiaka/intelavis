<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="IntelAvis pour vos enquêtes de satisfactions">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/PNG" sizes="16x16" href="{{ asset('plugins/images/favicon.PNG')}}">
    <title>Intel avis - admin</title>
    <link href="{{asset('plugins/components/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet">
    <!-- ===== Bootstrap CSS ===== -->
    <link href="{{ asset('bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet">

    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/vue-input-tag"></script>


    <!-- ===== Plugin CSS ===== -->
    <link rel="stylesheet" href="{{asset('plugins/components/dropify/dist/css/dropify.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/components/simplebar/simplebar.css')}}">
    <link href="{{ asset('plugins/components/icheck/skins/all.css')}}" rel="stylesheet">
    <link href="{{ asset('plugins/components/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet">
    <link href="{{ asset('plugins/components/morrisjs/morris.css')}}" rel="stylesheet">
    <link href="{{ asset('plugins/components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet">
    <!-- ===== Animation CSS ===== -->
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <!-- ===== Custom CSS ===== -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/w3.css')}}" rel="stylesheet">
    <!-- ===== Color CSS ===== -->
    <link href="{{asset('css/colors/default.css')}}" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>