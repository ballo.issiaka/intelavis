
@in


<aside class="sidebar">
    <div class="scroll-sidebar">
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div class="profile-image">
                    <i class ="fa fa-user fa-5x img-circle"></i>
                    <a href="javascript:void(0);" class="dropdown-toggle u-dropdown text-blue" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="badge badge-danger">
                                    <i class="fa fa-angle-down"></i>
                                </span>
                    </a>
                    <ul class="dropdown-menu animated flipInY">
                        <li><a href="javascript:void(0);"><i class="fa fa-user"></i> Profile</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="javascript:void(0);"><i class="fa fa-cog"></i> Paramètre du compte</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ route('logout') }}"><i class="fa fa-power-off"></i> Deconnexion</a></li>
                    </ul>
                </div>
                <p class="profile-text m-t-15 font-16"> {{ Auth::user()->name }}</p>
            </div>
        </div>
        <nav class="sidebar-nav">
            <ul id="side-menu">
                <li>
                    <a class="@if(Request::path() == 'dashboard') active @endif waves-effect" href="{{ route('dashboard.show') }}" aria-expanded="false"><i class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Tableau de bord </span></a>
                </li>
                <li>
                    <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-note fa-fw fa-fw"></i> <span class="hide-menu"> Sondage </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('sondages.create')}}">Ajouter un sondage</a></li>
                        <li><a href="{{route('sondages.index')}}">Listes des sondages</a></li>
                    </ul>
                </li>
                <li>
                    <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-screen-tablet fa-fw"></i> <span class="hide-menu"> Tablettes</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('groupesdevices.index')}}">Groupe</a></li>
                    </ul>
                </li>
                <li>
                    <a class="waves-effect" href="#" onclick="alert('bientôt disponible');" aria-expanded="false"><i class="icon-globe-alt fa-fw"></i> <span class="hide-menu"> Web</span></a>
                </li>

                <li>
                    <a class="waves-effect" href="#" onclick="alert('bientôt disponible');" aria-expanded="false"><i class="icon-envelope-open fa-fw"></i> <span class="hide-menu"> Email</span></a>
                </li>
            </ul>
        </nav>
    </div>
</aside>