<!DOCTYPE html>
<html lang="en">

@include("back.common.head")

<body class="mini-sidebar">
<!-- ===== Main-Wrapper ===== -->

<div id="sondage">
    <div id="wrapper">
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <!-- ===== Top-Navigation ===== -->
    @include("back.common.topNavigation")
    <!-- ===== Top-Navigation-End ===== -->
        <!-- ===== Left-Sidebar ===== -->
    @include("back.common.sidebar")
    <!-- ===== Left-Sidebar-End ===== -->
        <!-- ===== Page-Content ===== -->
        <div class="page-wrapper">
            <!-- ===== Page-Container ===== -->

        @yield('content')

        <!-- ===== Page-Container-End ===== -->
            @include('back.common.footer')
        </div>
        <!-- ===== Page-Content-End ===== -->
    </div>
</div>

<!-- ===== Main-Wrapper-End ===== -->
<!-- ==============================
    Required JS Files
=============================== -->
@include("back.common.script")
@yield('scripts')
</body>

</html>
