


@extends('back.layout.app')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="white-box">

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('groupesdevices.update',$groupe->id) }}" >
                            {{ method_field('PUT') }}
                            @csrf
                            <div class="form-body">
                                <h3 class="box-title">{{ $groupe->libelle }}</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group @if($errors->has('titre')) has-error @endif">
                                            <label class="control-label">Titre du groupe</label>
                                            <input type="text" id="titre" name="titre" class="form-control" value="{{ $groupe->libelle }}" placeholder="titre">
                                            @if($errors->has('titre'))
                                                <span class="help-block"> Titre non conforme. </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group @if($errors->has('sondage')) has-error @endif ">
                                            <label class="control-label">Sondage</label>
                                            <select class="form-control" name="sondage">
                                                @foreach($sondages as $sondage)
                                                    <option value="{{ $sondage->id }}" @if($groupe->sondage_id === $sondage->id) selected='selected' @endif> {{ $sondage->titre }}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('sondage'))
                                                <span class="help-block"> Sondage non conforme. </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Modifier</button>
                            </div>
                        </form>
                    </div>



                </div>
            </div>
            <div class="col-md-8">
                 <div class="white-box">
                     <h3 class="box-title">Liste des tablettes</h3>
                     <p class="text-muted">Présente la liste des tablettes liées au groupe</p>
                     <div class="table-responsive">
                         <table class="table color-table primary-table color-bordered-table primary-bordered-table">
                             <thead>
                             <tr>
                                 <th>#</th>
                                 <th>Model</th>
                                 <th>OS</th>
                             </tr>
                             </thead>
                             <tbody>
                             @foreach($mobiles as $key=>$mobile)
                                 <input type="hidden" value="{{ $key++ }}">
                             <tr>
                                 <td>{{ $key }}</td>
                                 <td>{{ $mobile->model }}</td>
                                 <td>{{ $mobile->plateforme }}</td>
                             </tr>
                                 @endforeach
                             </tbody>
                             <tfoot>
                              {{ $mobiles->links() }}
                             </tfoot>
                         </table>
                     </div>

                 </div>
            </div>


            </div>
        </div>

    </div>

@endsection
<script>
    import InputTag from "vue-input-tag/src/components/InputTag";
    export default {
        components: {InputTag}
    }
</script>