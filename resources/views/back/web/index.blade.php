@extends('back.layout.app')

@section('content')

    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-md-12">
                <div class="white-box text-center">
                    <h3>SONDAGE WEB</h3>
                </div>
            </div>
        </div>

        <div class="row">
            @foreach($sondages as $sondage)
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ $sondage->titre }}
                            <div class="panel-action">


                            </div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body text-center">
                                <a href="{{route('web.sondageweb',$sondage->id)}}">
                                    <img src="{{asset('images/admin/survey.png')}}" width="110" height="110">
                                </a>
                            </div>
                            <div class="panel-footer text-center">

                                <div class="btn-group">
                                    <a class="btn btn-primary btn-email" target="_blank" href="{{$sondage->getUrlAttribute()}}">Lien du Sondage</a>
                                    <button type="button" class="btn btn-default btn-copy js-tooltip js-copy" data-toggle="tooltip" data-placement="bottom"  title="Copier le lien!">
                                        <i class="icon-share-alt"></i>
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>

@endsection

@section('scripts')

    <script>

        function copyToClipboard(text, el) {
            var copyTest = document.queryCommandSupported('copy');
            var elOriginalText = el.attr('data-original-title');

            if (copyTest === true) {
                var copyTextArea = document.createElement("textarea");
                copyTextArea.value = text;
                document.body.appendChild(copyTextArea);
                copyTextArea.select();
                try {
                    var successful = document.execCommand('copy');
                    var msg = successful ? 'Copié!' : 'Whoops, not copied!';
                    el.attr('data-original-title', msg).tooltip('show');
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
                document.body.removeChild(copyTextArea);
                el.attr('data-original-title', elOriginalText);
            } else {
                // Fallback if browser doesn't support .execCommand('copy')
                window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
            }
        }

        $(document).ready(function() {
            // Initialize
            // ---------------------------------------------------------------------

            // Tooltips
            // Requires Bootstrap 3 for functionality
            $('.js-tooltip').tooltip();

            // Copy to clipboard
            // Grab any text in the attribute 'data-copy' and pass it to the
            // copy function
            $('.js-copy').click(function() {
                var text = $(this).attr('data-copy');
                var el = $(this);
                copyToClipboard(text, el);
            });
        });
    </script>

    @endsection

