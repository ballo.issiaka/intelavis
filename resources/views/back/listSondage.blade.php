@extends('back.layout.app')

@section('content')

    <div class="container-fluid">
        <div class="white-box text-center">
          <h1>SONDAGES</h1>
        </div>


    <div class="row white-box">
        <div class="col-md-2">
            <div class="row">
                <div class="col-md-4">

                </div>
                <div class="col-md-4">
                    <br> <br>
                    <button type="button" class="btn btn-danger btn-circle btn-xl"><i class="fa fa-plus"></i> </button>
                </div>
            </div>
        </div>
        <div class="col-md-10">
               <div class="row">
                   <div class="col-md-3 ">
                       <div class="white-box text-center">
                           <div>
                               <img src="{{asset('images/admin/survey.png')}}" width="100" height="100">
                           </div> <br>
                           <div>
                               PARTICIPANT 500
                           </div>
                       </div>
                   </div>

                   <div class="white-box upload-widget col-md-4">
                       <div class="t-a-c">
                           <div class="complete-chart" id="graph1" data-percent="45" data-size="160" data-line="5" data-rotate="0" data-color="#e74a25" data-text-color="#e74a25"><h1 class="per-number" style="color: rgb(231, 74, 37);">45<span class="per-icon">%</span></h1><canvas height="160" width="160"></canvas></div>
                           <p class="font-20 m-b-0">Uploading</p>
                           <p class="text-primary font-semibold font-18">Myphotos_friends.png</p>
                           <span class="up-speed m-t-20 m-b-10">Speed: <span class="text-primary font-semibold">450 kbps</span></span>
                       </div>
                   </div>


               </div>

        </div>


    </div>

    </div>

    @endsection

@section('scripts')

    @endsection
