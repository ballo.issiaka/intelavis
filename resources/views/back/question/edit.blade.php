@extends('back.layout.app')


@section('content')

    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Default Basic Forms</h3>
                    <p class="text-muted m-b-30 font-13"> All bootstrap element classies </p>
                    <form class="form-horizontal" method="POST" action="{{ route('questions.update',$question->id) }}" >
                        {{ method_field('PUT') }}
                        @csrf
                        <div class="form-group">
                            <label class="col-md-12">Titre</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" value="{{ $question->titre_question }}"  name="titre_question">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Logo</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" value="1" name="type_reponse_id">
                            </div>
                        </div>

                        <h3>selection d'une option</h3>
                        @foreach($question->options as $indexkey=> $option)
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-12">choisir un logo</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" value="" name="option[0][logo]">
                                </div>
                            </div><div class="form-group">
                                <label class="col-md-12">libelle</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" value="{{$option->titre_option}}" name="option[{{$indexkey}}][libelle]">
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <button type="submit" > envoyer</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- ===== Right-Sidebar-End ===== -->
    </div>



@endsection