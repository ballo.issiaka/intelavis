@extends('back.layout.app')


@section('content')

    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Default Basic Forms</h3>
                    <p class="text-muted m-b-30 font-13"> All bootstrap element classies </p>
                    <form class="form-horizontal" method="POST" action="{{ route('questions.store') }}" >
                        @csrf
                        <div class="form-group">
                            <label class="col-md-12">Titre question</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" value=""  name="titre_question">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">type reponse</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" value="" name="type_reponse_id">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">sondage</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" value="{{$sondage->id}}" name="sondage_id">
                            </div>
                        </div>
                        <hr>
                        <h3>selection d'une option</h3>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-12">choisir un logo</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" value="" name="option[0][logo]">
                                </div>
                            </div><div class="form-group">
                                <label class="col-md-12">libelle</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" value="" name="option[0][libelle]">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-12">choisir un logo</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" value="" name="option[1][logo]">
                                </div>
                            </div><div class="form-group">
                                <label class="col-md-12">libelle</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" value="" name="option[1][libelle]">
                                </div>
                            </div>
                        </div>

                        <button type="submit" > envoyer</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- ===== Right-Sidebar-End ===== -->
    </div>
    <script>

    </script>



@endsection