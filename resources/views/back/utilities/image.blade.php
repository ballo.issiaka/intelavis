<input type='file' onchange="readURL(this);" />
<img v-bind:id="'d'+drop" src="http://placehold.it/180" alt="your image" />

<div class="hidden">{{$data['id']}}</div>
@section('scripts')

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    let id = $('.hidden').html();
                    $('#d'+id)
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection