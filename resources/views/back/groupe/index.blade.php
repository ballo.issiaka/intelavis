


@extends('back.layout.app')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading"> Création du groupe</div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('groupesdevices.store') }}">
                                @csrf
                                @include('back.layout.messages')
                                <div class="form-body">
                                    <h3 class="box-title">GROUPE</h3>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group @if($errors->has('titre')) has-error @endif">
                                                <label class="control-label">Titre du groupe</label>
                                                <input type="text" id="titre" name="titre" class="form-control" value="{{old('titre')}}" placeholder="titre">
                                                @if($errors->has('titre'))
                                                    <span class="help-block"> Titre non conforme. </span>
                                                @endif
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group @if($errors->has('sondage')) has-error @endif ">
                                                <label class="control-label">Sondage</label>
                                                <select class="form-control" name="sondage">
                                                    @foreach($sondages as $sondage)
                                                        <option value="{{ $sondage->id }}" >{{ $sondage->titre }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('sondage'))
                                                    <span class="help-block"> Sondage non conforme. </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Enregistrer</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading"> Liste des groupes</div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th>Titre</th>
                                        <th>Sondage</th>
                                        <th class="text-nowrap">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($groupes as $key=>$groupe)
                                        <input type="hidden" {{ $key++ }}>
                                    <tr>
                                        <td> {{ $key }}</td>
                                        <td>{{$groupe->libelle}}</td>
                                        <td>{{ $groupe->sondage->titre }}</td>
                                        <td class="text-nowrap">
                                            <a href="{{route('devices.view',$groupe->id)}}" data-toggle="tooltip" data-original-title="Edit" > <i class="fa fa-mobile-phone text-inverse m-r-10"></i> </a>
                                            <a href="#deleteGroupe" data-toggle="modal" data-original-title="Close" data-groupe-id="{{$groupe->id}}"> <i class="fa fa-close text-danger"></i> </a>
                                            <a href="{{route('devices.stat',$groupe->id)}}" > <i class="fa fa-newspaper-o text-default"></i> </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>

                                     {{ $groupes->links() }}

                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="deleteGroupe" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Supprimer</h4> </div>
                <div class="modal-body">
                    <h4>Ête vous de supprimer ce groupe <span id="sondageTitre"> </span>?</h4>
                </div>
                <div class="modal-footer">
                    <form id="userForm" action="" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-danger">Supprimer</button>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


@endsection


@section('scripts')

    <script>

        $('#deleteGroupe').on('show.bs.modal', function(e) {


            var groupeId = $(e.relatedTarget).data('groupe-id');
            //var sondageTitre = $(e.relatedTarget).data('sondage-titre');


            $('#userForm').attr("action", "{{ url('/groupesdevices') }}" + "/" + groupeId);
          //  $('#sondageTitre').val(sondageTitre)
        });
    </script>

    @endsection