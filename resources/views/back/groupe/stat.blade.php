@extends('back.layout.app')
@section('content')
        <div class="container-fluid">
            <div class="w3-container m-t-20 ">
                <div class="row white-box">
                    <div class="row" id="MainStat">
                        <div class="col-md-12 row">
                            <div class="col-md-12">
                            </div>
                            @if($globalStat["smile"]["happy"] + $globalStat["smile"]["sad"] > 0)
                                <div class="col-md-12 row">
                                    <div class="col-md-6 p-10 white-box" style="height: 220px; width:50%">
                                        <div class="col-md-6">
                                            <div class="complete-chart" id="graph1"
                                                 data-percent="{{round((($globalStat["smile"]["happy"] * 100) / ($globalStat["smile"]["happy"] + $globalStat["smile"]["sad"])),2)}}"
                                                 data-size="160" data-line="10"
                                                 data-rotate="0"
                                                 data-text="😄"
                                                 data-color="#e74a25" data-text-color="#e74a25">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h2>{{round((($globalStat["smile"]["happy"] * 100) / ($globalStat["smile"]["happy"] + $globalStat["smile"]["sad"])),2)}}
                                                %</h2>
                                        </div>
                                    </div>
                                    <div class="col-md-6 row p-10 white-box" style="height: 220px; width:50%">
                                        <div class="col-md-6">
                                            <div class="complete-chart" id="graph2"
                                                 data-percent="{{round((($globalStat["smile"]["sad"] * 100) / ($globalStat["smile"]["happy"] + $globalStat["smile"]["sad"])),2)}}"
                                                 data-size="160" data-line="10"
                                                 data-rotate="0"
                                                 data-text="😞"
                                                 data-color="#e74a25" data-text-color="#e74a25">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h2>{{round((($globalStat["smile"]["sad"] * 100) / ($globalStat["smile"]["happy"] + $globalStat["smile"]["sad"])),2)}}
                                                %</h2>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if(($globalStat["trueFalse"]["yes"] + $globalStat["trueFalse"]["no"]) > 0)
                                <div class="col-md-12 row">
                                    <div class="col-md-6 p-10  white-box" style="height: 220px; width:50%">
                                        <div class="col-md-6">
                                            <div class="complete-chart" id="graph1smileyes"
                                                 data-percent="{{round((($globalStat["trueFalse"]["yes"] * 100) / ($globalStat["trueFalse"]["yes"] + $globalStat["trueFalse"]["no"])),2)}}"
                                                 data-size="160" data-line="10"
                                                 data-rotate="0"
                                                 data-text="✔"
                                                 data-color="#e74a25" data-text-color="#e74a25">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h2>{{round((($globalStat["trueFalse"]["yes"] * 100) / ($globalStat["trueFalse"]["yes"] + $globalStat["trueFalse"]["no"])),2)}}%</h2>
                                        </div>
                                    </div>
                                    <div class="col-md-6 row p-10 white-box" style="height: 220px; width:50%">
                                        <div class="col-md-6">
                                            <div class="complete-chart" id="graphsmileNo"
                                                 data-percent="{{round((($globalStat["trueFalse"]["no"] * 100) / ($globalStat["trueFalse"]["yes"] + $globalStat["trueFalse"]["no"])),2)}}"
                                                 data-size="160" data-line="10"
                                                 data-rotate="0"
                                                 data-text="✘"
                                                 data-color="#e74a25" data-text-color="#e74a25">
                                                <canvas height="160" width="160"></canvas>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h2>{{round((($globalStat["trueFalse"]["no"] * 100) / ($globalStat["trueFalse"]["yes"] + $globalStat["trueFalse"]["no"])),2)}}%</h2>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>


                    </div>
                    <div class="row">

                        @if($nbr_reponse > 0)
                            @foreach($questions_array as $key=>$question)


                                @switch ($question['type_reponse_id'])
                                    @case(1)
                                    @include('back.sondage.overview_inc.case1')
                                    @break
                                    @case(2)
                                    @include('back.sondage.overview_inc.case2')
                                    @break
                                    @case(3)
                                    @include('back.sondage.overview_inc.case3')
                                    @break
                                    @case(4)
                                    @include('back.sondage.overview_inc.case4')
                                    @break
                                    @case(5)
                                    @include('back.sondage.overview_inc.case5')
                                    @break
                                    @case(6)
                                    @include('back.sondage.overview_inc.case6')
                                    @break
                                    @case(7)
                                    @include('back.sondage.overview_inc.case7')
                                    @break
                                    @case(8)
                                    @include('back.sondage.overview_inc.case8')
                                    @break
                                @endswitch

                            @endforeach

                        @endif


                    </div>
                </div>
            </div>
        </div>

@endsection
@section('scripts')
@endsection