@extends('back.layout.app')


@section('content')

    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Creer un groupe</h3>
                    <p class="text-muted m-b-30 font-13"> All bootstrap element classies </p>
                    <form class="form-horizontal" method="POST" action="{{ route('groupesdevices.store') }}" >
                        @csrf
                        <div class="form-group">
                            <label class="col-md-12">Titre du groupe</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" value=""  name="libelle">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Sondage</label>
                                <select class="form-control" data-placeholder="le sondage" tabindex="1" name="sondage" >
                                    @foreach($sondages as $sondage)
                                    <option value="{{ $sondage->id }}">{{ $sondage->titre }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <button type="submit" > envoyer</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- ===== Right-Sidebar-End ===== -->
    </div>
    <script>

    </script>



@endsection