@extends('back.layout.app')

@section('content')

    <div class="container-fluid">

        <div class="white-box text-center">
            <h1>ETAT DES EMAILS</h1>
        </div>

        <div class="white-box">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Email</th>
                        <th>Message</th>
                        <th class="text-nowrap">Statut</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($datas as $key=>$data )
                       <input type="hidden" value="{{ $key++ }}">
                    <tr>
                        <td>{{$key}}</td>
                        <td>{{ $data->email }}</td>
                        <td>{{ $data->message }}</td>
                        <td class="text-nowrap">
                            @if($data->is_use == 1)
                            <div class="label label-table label-success">Terminer</div>
                             @else
                                <div class="label label-table label-danger">Non terminer</div>
                                @endif
                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                    {{ $datas->links() }}
                    </tfoot>
                </table>
            </div>
        </div>


    </div>





@endsection

