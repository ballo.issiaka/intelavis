@extends('back.layout.app')

@section('content')

        <div class="container-fluid">
            <!-- .row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box text-center">
                        <h3>SONDAGE EMAIL</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach($sondages as $sondage)
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ $sondage->titre }}
                            <div class="panel-action">


                            </div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body text-center">
                                <a href="{{ route('email.sondagemail',$sondage->id) }}">
                                    <img src="{{asset('images/admin/survey.png')}}" width="110" height="110">
                                </a>
                            </div>
                            <div class="panel-footer">
                                <div class="button-box text-center">
                                    <button class="fcbtn btn btn-primary btn-outline btn-1e " onclick="location.href='{{route('customeremail.form',$sondage->id)}}'">Envoi de mail</button>
                                    <button class="fcbtn btn btn-success btn-outline btn-1e" onclick="location.href='{{route('mails.sondage',$sondage->id)}}'"> Mail envoyé</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                    @endforeach
            </div>

        </div>





@endsection

