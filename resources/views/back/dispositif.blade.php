@extends('back.layout.app')

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-6">
           <div class="white-box">

               <h3 class="box-title m-b-0">Sample Forms with icon</h3>
               <p class="text-muted m-b-30 font-13"> Bootstrap Elements </p>
               <div class="row">
                   <div class="col-sm-12 col-xs-12">
                       <form>
                           <div class="form-group">
                               <label for="exampleInputuname">Nom du groupe</label>
                               <div class="input-group">
                                   <div class="input-group-addon"><i class="ti-menu"></i></div>
                                   <input type="text" class="form-control" id="exampleInputuname" placeholder="Nom du groupe"> </div>
                           </div>
                           <div class="form-group">
                               <label for="exampleInputEmail1">Mot de passe du dispositif</label>
                               <div class="input-group">
                                   <div class="input-group-addon"><i class="ti-key"></i></div>
                                   <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Entrer le mot de passe"> </div>
                           </div>

                           <div class="form-group">
                               <label class="control-label">Choix du sondage</label>
                               <select class="form-control" data-placeholder="Sondage" tabindex="1">
                                   <option value="Category 1">Category 1</option>
                                   <option value="Category 2">Category 2</option>
                                   <option value="Category 3">Category 5</option>
                                   <option value="Category 4">Category 4</option>
                               </select>
                           </div>

                               <div class="text-center">
                                   <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Enregistrer</button>
                               </div>
                       </form>
                   </div>
               </div>

           </div>
        </div>
        <div class="col-md-6">
            <div class="white-box">

            </div>
        </div>
    </div>


</div>


    @endsection