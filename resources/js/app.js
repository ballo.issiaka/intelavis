
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


window.Vue = require('vue');
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/createForm.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('create-form', require('./components/customeremail/createForm.vue').default);
Vue.component('Imgctrl', require('./components/questions/imgctrl.vue').default);
Vue.component('lvl2_1', require('./components/questions/lvl2_1.vue').default);
Vue.component('questions', require('./components/questions/questions.vue').default);
Vue.component('stat', require('./components/Statistique/stat.vue').default);
Vue.component('case1', require('./components/Statistique/cas/case1.vue').default);
Vue.component('case2', require('./components/Statistique/cas/case2.vue').default);
Vue.component('case3', require('./components/Statistique/cas/case3.vue').default);
Vue.component('case4', require('./components/Statistique/cas/case4.vue').default);
Vue.component('case5', require('./components/Statistique/cas/case5.vue').default);
Vue.component('case6', require('./components/Statistique/cas/case6.vue').default);
Vue.component('sousquestion', require('./components/questions/sousquestion.vue').default);
Vue.component('sondage', require('./components/front/sondage.vue').default);
Vue.component('option_one', require('./components/front/option_one.vue').default);
Vue.component('qcm_texte', require('./components/front/qcmtexte_component.vue').default);
Vue.component('trueordare', require('./components/front/trueordare.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#sondage',

});
