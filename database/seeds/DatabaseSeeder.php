<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        factory(App\Sondage::class,4)->create()->each( function ($sondage){
//             $sondage->questions()
//                 ->saveMany(
//                     factory(App\Question::class, rand(5,10))->make()
//                 );
//
//
//        });

        factory(App\User::class,4)->create();
    }
}
