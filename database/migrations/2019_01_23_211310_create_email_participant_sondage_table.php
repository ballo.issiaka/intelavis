<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailParticipantSondageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_participant_sondage', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('email_participant_id');
            $table->unsignedInteger('sondage_id');
            $table->boolean('is_use')->default(0);
            $table->string('token');
            $table->text('message');


            $table->foreign('email_participant_id')->references('id')->on('email_participants')->onDelete('cascade');
            $table->foreign('sondage_id')->references('id')->on('sondages')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_participant_sondage');
    }
}
