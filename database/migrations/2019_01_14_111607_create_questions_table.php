<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titre_question');
            $table->boolean('is_sousquestion');
            $table->boolean('mandatory')->default(0);
            $table->unsignedInteger('type_reponse_id');
            $table->unsignedInteger('sondage_id');
            $table->timestamps();


            $table->foreign('type_reponse_id')->references('id')->on('type_reponses')->onDelete('cascade');
            $table->foreign('sondage_id')->references('id')->on('sondages')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
