<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSondagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sondages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titre');
            $table->string('logo');
            $table->text('start_text');
            $table->text('end_text');
            $table->text('couleur');
            $table->boolean('web')->default(0);
            $table->boolean('mobile')->default(0);
            $table->boolean('email')->default(0);
            $table->boolean('anonyme');
            $table->unsignedInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sondages');
    }
}
