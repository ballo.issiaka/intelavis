<?php

use Faker\Generator as Faker;

$factory->define(App\Sondage::class, function (Faker $faker) {
    return [
         'titre'=>rtrim($faker->sentence(rand(5,10)),'.'),
         'logo'=>$faker->imageUrl(),
        'start_text'=>$faker->sentence,
        'end_text'=>$faker->sentence,


    ];
});
