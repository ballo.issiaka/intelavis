<?php

use Faker\Generator as Faker;

$factory->define(App\Question::class, function (Faker $faker) {
    return [
        'titre_question'=>rtrim($faker->sentence(rand(5,10)),'.')." ?",
        'is_sousquestion'=>0,
        'type_reponse_id'=>1,

    ];
});
