<?php

use Faker\Generator as Faker;

$factory->define(App\Option::class, function (Faker $faker) {
    return [
        'titre_option'=>$faker->sentences(rand(5,4)),
        'has_subquestion'=>0,
        'default_option_id'=>1,


    ];
});
