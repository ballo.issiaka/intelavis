<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    protected $fillable = ['titre_question','type_reponse_id','is_sousquestion','sondage_id','mandatory'];

    public function type()
    {

        return $this->belongsTo(TypeReponse::class);
    }
    public function sondage()
    {
        return $this->belongsTo(Sondage::class);
    }

    public function options()
    {

        return $this->hasMany(Option::class);
    }//

    //
    public function optionsMany(){

        return $this->belongsToMany(Options::class);

    }


}
