<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeReponse extends Model
{

    protected $fillable = ['libelle_type'];

    public function questions()
    {

        return $this->hasMany(Question::class);
    }//


}
