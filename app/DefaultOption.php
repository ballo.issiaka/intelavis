<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DefaultOption extends Model
{
    protected $fillable = ['libelle'];

    public function options(){

        return $this->hasMany(Option::class);
    }
}
