<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sondage extends Model
{

    protected $fillable = ['titre','logo','start_text','end_text','user_id','couleur','web','mobile','email','anonyme'];
    //



    public function getUrlAttribute()
    {
        return route("web.viewsondage", $this->id);
    }



    public function questions()
    {

        return $this->hasMany(Question::class)->orderBy('id');
    }//


    public function getAttributeTitre()
    {
        return $this->titre;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function reponses(){
        return $this->hasMany(Reponse::class);
    }



    public function groupeDevices(){

        return $this->hasMany(GroupeDevice::class);
    }

    public function emails(){

        return $this->belongsToMany(EmailParticipant::class)->withPivot('token','is_use','url')->withTimestamps();
    }
}
