<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailParticipant extends Model
{
    protected $fillable = ['email','user_id'];

    public function user(){

        return $this->belongsTo(User::class);
    }

    public function sondages(){
       return $this->belongsToMany(Sondage::class)->withPivot('token','is_use','url')->withTimestamps();
    }
}
