<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{

    protected $fillable = ['titre_option','has_subquestion','default_option_id','question_id'];
    //
    public function question()
    {

        return $this->belongsTo(Question::class);
    }
    public function defaultOption()
    {

        return $this->belongsTo(DefaultOption::class);
    }




    //BelongsToMany
    public function questionsMany(){

        return $this->belongsToMany(Question::class);

    }


}
