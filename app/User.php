<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','prenom','numero','zip_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function pays(){

        return $this->belongsTo(Pays::class);
    }

    public function categorieStructure(){

        return $this->belongsTo(CategorieStructure::class);
    }

    public function abonnement(){

        return $this->belongsTo(Abonnement::class);
    }

    public function sondages(){

        return $this->hasMany(Sondage::class);
    }

    public function groupes(){

        return $this->hasMany(GroupeDevice::class);
    }

    public function emails(){

        return $this->hasMany(EmailParticipant::class);
    }




}
