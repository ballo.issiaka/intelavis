<?php

namespace App\Http\Controllers\Api;

use App\Device;
use App\GroupeDevice;
use App\Option;
use App\Participant;
use App\Question;
use App\QuestionInfo;
use App\Reponse;
use App\Sondage;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;

class MobileController extends Controller
{

    //StoreMobile
    public function storeGetDevice(Request $request){
       $device = new Device();
       $device->model = $request->model;
       $device->plateforme = $request->plateforme;
       $device->groupe_device_id = $request->groupeid;
       $device->save();
       $device->toArray();

           return response()->json($device, 200);
    }

    //delete Mobile
    public function deleteDevice($deviceid){
        $device = Device::findOrFail($deviceid);
        $device->delete();

        return response()->json(array('succes'=>true, 'message'=>'device supprimer'),200);

    }

    // Verifier si le mobile peut se logger
    public function loginMobile(Request $request){

       $email = $request->email;
       $password = $request->password;

        $user = User::where('email', '=', $email)->first();





        if ($user === null){
            return response()->json(array('error' => true,'message'=>'utilisateur existe pas'), 404);

        }

        if(!Hash::check($password,$user->password)){

            return response()->json(array('error' => true,'message'=>'mot de passe incorrect'), 404);
        }

  /*      $abn = $user->abonnement->max_devices;


        $allgroupe = $user->groupes;
        $nbdevices = 0;
        foreach ($allgroupe as $groupe){
            $nbdevices +=  $groupe->devices->count();

        }

        if($nbdevices >= $abn){
            return response()->json(array('error' => true,'message'=>'nombre de device maximim enregistrer'), 404);
        }*/



       //$groupesDevices = User::findOrFail($user->id)->groupes->toArray();
       // $utilisateur = User::findOrFail($user->id)->toArray();


        return  response()->json(array('message'=>true,'message'=>'bien connecter'),200);

    }

    public function getUser($email){
        $user = User::where('email', '=', $email)->first();
        $utilisateur = User::findOrFail($user->id)->toArray();

        return  response()->json($utilisateur,200);
    }


    public function indexGroupe( $id){

        $groupesDevices = User::findOrFail($id)->groupes->toArray();

        foreach ( $groupesDevices as $key=>$groupesDevice){
            $sondage = GroupeDevice::findOrFail($groupesDevice['id'])->sondage->toArray();
            $groupesDevices[$key]['sondage']= $sondage['titre'];
        }

        return response()->json($groupesDevices,200);
    }


    public function sondageGroupe($id){


           $sondage = GroupeDevice::findOrFail($id)->sondage->toArray();

           $questions = Sondage::findOrfail($sondage['id'])->questions->toArray();



           $sondage['questions'] = $questions;

           foreach ($questions as $key =>$question){
               $options = Question::findOrfail($question['id'])->options->toArray();
               $sondage["questions"][$key]["options"] = $options;
               //$optquestion = Question::findOrfail($question['id'])->options->option_question->toArray();

               foreach ($options as $key2 => $option){

                   $sousquestion= Option::findOrfail($option['id'])->questionsMany->first();

                   $sondage["questions"][$key]["options"][$key2]["sondageid"] =  $sondage["questions"][$key]['sondage_id'];
                   $sondage["questions"][$key]["options"][$key2]["sousquestions"] = $sousquestion["id"];
               }

           }
           //$reponse =  Response::json(array('sondages'=>$sondages,'questions'=>$questions,'options'=>$options,'sousquestions'=>$sousquestion));
           return Response::json($sondage);


    }

    public function storeReponse(Request $request){


        $questionsList =  json_decode($request->questions) ;

         $sondageId =  $questionsList[0]->sondage_id;

         $sondage = Sondage::findOrFail($sondageId);

        if($sondage->anonyme == 1){
            $participant =new Participant();
            $participant->nom = $questionsList[0]->nom;
            $participant->email = $questionsList[0]->email;
            $participant->numero = $questionsList[0]->numero;
            $participant->save();
        }else{
            $participant =new Participant();
            $participant->save();
        }


       foreach ($questionsList as $ques){
           if($questionsList[0] != $ques && $sondage->anonyme == 1){
               $reponse = new Reponse();
               $reponse->answer = $ques->answer;
               $reponse->plateforme = "MOBILE";
               $reponse->group_id = $ques->groupe_id;
               $reponse->question_id = $ques->id;
               $reponse->participant_id = $participant->id;
               $reponse->sondage_id = $ques->sondage_id;
               $reponse->save();
           }else if($sondage->anonyme == 0){
               $reponse = new Reponse();
               $reponse->answer = $ques->answer;
               $reponse->plateforme = "MOBILE";
               $reponse->group_id = $ques->groupe_id;
               $reponse->question_id = $ques->id;
               $reponse->participant_id = $participant->id;
               $reponse->sondage_id = $ques->sondage_id;
               $reponse->save();
           }
        }

        return response()->json($request->questions, 200);

    }
}
