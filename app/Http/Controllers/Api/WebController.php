<?php

namespace App\Http\Controllers\Api;

use App\Option;
use App\Question;
use Illuminate\Http\Request;
use App\Participant;
use App\Http\Controllers\Controller;
use App\Reponse;
use App\Sondage;
use Illuminate\Support\Facades\Auth;

class WebController extends Controller
{
    public function storeReponse(Request $request)
    {
        $questionsList =  json_decode($request->question) ;

        $sondageId =  $questionsList[0]->sondage_id;

        $sondage = Sondage::findOrFail($sondageId);

        if($sondage->anonyme == 1){
            $participant =new Participant();
            $participant->nom = $questionsList[0]->nom;
            $participant->email = $questionsList[0]->email;
            $participant->numero = $questionsList[0]->contact;
            $participant->save();
        }else{
            $participant =new Participant();
            $participant->save();
        }


        foreach ($questionsList as $ques){

            if($questionsList[0] != $ques && $sondage->anonyme == 1){
                $reponse = new Reponse();
                $reponse->answer = $ques->answer;
                $reponse->plateforme = "WEB";
                $reponse->question_id = $ques->id;
                $reponse->participant_id = $participant->id;
                $reponse->sondage_id = $ques->sondage_id;
                $reponse->save();
            }else if($sondage->anonyme == 0){
                $reponse = new Reponse();
                $reponse->answer = $ques->answer;
                $reponse->plateforme = "WEB";
                $reponse->question_id = $ques->id;
                $reponse->participant_id = $participant->id;
                $reponse->sondage_id = $ques->sondage_id;
                $reponse->save();
            }
        }

        return response()->json($request->questions, 200);
    }
}
