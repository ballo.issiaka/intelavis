<?php

namespace App\Http\Controllers\Api;

use App\Option;
use App\Participant;
use App\Question;
use App\Reponse;
use App\Sondage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class SondageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $sondages = Sondage::findOrFail($id)->toArray();

       $questions = Sondage::findOrfail($id)->questions->toArray();

      // $groupedevice = Sondage::findOrfail($id)->groupeDevices;

       $sondages['questions'] = $questions;

       foreach ($questions as $key =>$question){
          $options = Question::findOrfail($question['id'])->options->toArray();
          $sondages["questions"][$key]["option"] = $options;
            //$optquestion = Question::findOrfail($question['id'])->options->option_question->toArray();

          foreach ($options as $key2 => $option){

              $sousquestion= Option::findOrfail($option['id'])->questionsMany->first();


              $sondages["questions"][$key]["option"][$key2]["sousquestions"] = $sousquestion["id"];
          }

       }

         //$reponse =  Response::json(array('sondages'=>$sondages,'questions'=>$questions,'options'=>$options,'sousquestions'=>$sousquestion));

       return Response::json($sondages);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
    public function statView(Request $request){

        $sondage = Sondage::find($request);
        dd($sondage);
        if($sondage->user_id != Auth::user()->id)
            abort(404);
        $match_close = ['sondage_id' => $sondage->id, 'is_sousquestion' => 0];
        $questions = Question::where($match_close)->get();
        $questions_array = $questions->toArray();
        $nbr_reponse = Sondage::findOrFail($sondage->id)->reponses->count();
        $sumOfYes = 0;
        $sumOfNo = 0;
        $sumOfGood = 0;
        $sumOfBad = 0;


        foreach ($questions_array as $key => $question) {
            //dd($question);
            $options = Option::where('question_id', $question['id'])->get()->toArray();
            $opt_repAll = Reponse::where('question_id', $question['id'])->get()->count();

            if ($question['type_reponse_id'] == 8) {
                $participant_ids = Reponse::select('participant_id')->where('sondage_id', $question['sondage_id'])->distinct('participant_id')->get()->toArray();
                $participant = array();
                foreach ($participant_ids as $participant_id) {
                    $part = Participant::find($participant_id)->toArray();
                    if ($part != null) {
                        $participant[] = $part;
                    }
                }
                $questions_array[$key]['participants'] = $participant;
                //dd($participant);
            }
            if (in_array($question['type_reponse_id'], [1, 2, 3, 4])) {
                //dd($options);
                $options = Option::where('question_id', $question['id'])->get()->toArray();
                $data = '[';
                $per = '[';
                $questions_array[$key]['options'] = $options;
                if ($opt_repAll > 0) {
                    foreach ($options as $key2 => $option) {
                        $opt_rep = Reponse::where('answer', $option['id'])->get()->count();
                        if ($option['default_option_id'] == 1 || $option['default_option_id'] == 2)
                            $sumOfGood += $opt_rep;
                        if ($option['default_option_id'] == 3 || $option['default_option_id'] == 4)
                            $sumOfBad += $opt_rep;
                        if ($option['default_option_id'] == 5)
                            $sumOfYes += $opt_rep;
                        if ($option['default_option_id'] == 6)
                            $sumOfNo += $opt_rep;
                        $percent = ($opt_rep * 100) / $opt_repAll;
                        $questions_array[$key]['options'][$key2]['stat'] = round($percent, 2);

                        if ($options[count($options) - 1] == $option) {
                            $data .= '"' . $option['titre_option'] . '"]';
                            $per .= '"' . round($percent, 2) . '"]';
                        } else {
                            $data .= '"' . $option['titre_option'] . '",';
                            $per .= '"' . round($percent, 2) . '",';
                        }
                        $questions_array[$key]['data'] = $data;
                        $questions_array[$key]['per'] = $per;

                        //dd($percent);
                    }
                    //dd($sumOfGood);

                }

            }
            if (in_array($question['type_reponse_id'], [9,10])) {
                //dd($options);
                $data = '[';
                $per = '[';
                $questions_array[$key]['options'] = $options;
                $opt_repAll = 0;
                foreach($options as $opt){
                    //dd($opt);
                    $opt_repAll += Reponse::where([['question_id', $question['id']],['answer','LIKE','%'.$opt['id'].'%']])->get()->count();
                }
                if ($opt_repAll > 0) {

                    foreach ($options as $key2 => $option) {
                        $opt_rep = Reponse::where([['question_id', $question['id']],['answer','LIKE','%'.$option['id'].'%']])->get()->count();
                        $percent = ($opt_rep * 100) / $opt_repAll;
                        $questions_array[$key]['options'][$key2]['stat'] = round($percent, 2);

                        if ($options[count($options) - 1] == $option) {
                            $data .= '"' . $option['titre_option'] . '"]';
                            $per .= '"' . round($percent, 2) . '"]';
                        } else {
                            $data .= '"' . $option['titre_option'] . '",';
                            $per .= '"' . round($percent, 2) . '",';
                        }
                        $questions_array[$key]['data'] = $data;
                        $questions_array[$key]['per'] = $per;

                        //dd($percent);
                    }
                    //dd($sumOfGood);

                }

            }
            if (in_array($question['type_reponse_id'], [5, 6, 7])) {
                $quest_rep = Reponse::select('answer')->where('question_id', $question['id'])->get()->toArray();
                $questions_array[$key]['reponses'] = $quest_rep;

            }

        }


        $globalStat = ["smile" => ["happy" => $sumOfGood, "sad" => $sumOfBad], "trueFalse" => ["yes" => $sumOfYes, "no" => $sumOfNo]];
        $statData=["sondage" => $sondage, "questions" => $questions, "questions_array" => $questions_array, "globalStat" => $globalStat, "nbr_reponse" => $nbr_reponse];
        // add something
        //dd($sumOfNo);
        return response()->json($statData, 200);
    }
}
