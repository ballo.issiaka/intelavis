<?php

namespace App\Http\Controllers\Admin;

use App\Device;
use App\GroupeDevice;
use App\Http\Requests\SondageRequest;
use App\Participant;
use App\Sondage;
use App\Question;
use App\Option;
use App\Reponse;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class SondageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $sondages = User::findOrfail(Auth::user()->id)->sondages;


        return view('back.sondage.index', compact('sondages'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.sondage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        request()->validate([
            'titre' => 'required',
            'logo' => 'required',
            'start' => 'required',
            'end' => 'required'

        ]);


        $sondage = new Sondage();
        $imageName = time() . '.' . request()->logo->getClientOriginalExtension();
        request()->logo->move(public_path('images/admin/sondages'), $imageName);
        $sondage->logo = $imageName;
        $sondage->titre = $request->titre;
        $sondage->start_text = $request->start;
        $sondage->end_text = $request->end;
        $sondage->web = $request->web;
        $sondage->mobile = $request->mobile;
        $sondage->email = $request->mail;
        $sondage->couleur = $request->couleur;
        $sondage->anonyme = $request->anonyme;
        $sondage->user_id = Auth::user()->id;
        $sondage->save();

        if ($request->anonyme == 1) {
            $question = new Question();
            $question->titre_question = "Votre contact s'il vous plaît !?";
            $question->type_reponse_id = 8;
            $question->is_sousquestion = 0;
            $question->sondage_id = $sondage->id;
            $question->mandatory = 1;
            $question->save();
        }


        return redirect()->route('sondages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sondage $sondage
     * @return \Illuminate\Http\Response
     */
    public function show(Sondage $sondage)
    {
        if($sondage->user_id != Auth::user()->id)
            abort(404);
        $match_close = ['sondage_id' => $sondage->id, 'is_sousquestion' => 0];
        $questions = Question::where($match_close)->get();
        $questions_array = $questions->toArray();
        $nbr_reponse = Sondage::findOrFail($sondage->id)->reponses->count();
        $sumOfYes = 0;
        $sumOfNo = 0;
        $sumOfGood = 0;
        $sumOfBad = 0;


        foreach ($questions_array as $key => $question) {
            //dd($question);
            $options = Option::where('question_id', $question['id'])->get()->toArray();
            $opt_repAll = Reponse::where('question_id', $question['id'])->get()->count();

            if ($question['type_reponse_id'] == 8) {
                $participant_ids = Reponse::select('participant_id')->where('sondage_id', $question['sondage_id'])->distinct('participant_id')->get()->toArray();
                $participant = array();
                foreach ($participant_ids as $participant_id) {
                    $part = Participant::find($participant_id)->toArray();
                    if ($part != null) {
                        $participant[] = $part;
                    }
                }
                $questions_array[$key]['participants'] = $participant;
                //dd($participant);
            }
            if (in_array($question['type_reponse_id'], [1, 2, 3, 4])) {
                //dd($options);
                $options = Option::where('question_id', $question['id'])->get()->toArray();
                $data = '[';
                $per = '[';
                $questions_array[$key]['options'] = $options;
                if ($opt_repAll > 0) {
                    foreach ($options as $key2 => $option) {
                        $opt_rep = Reponse::where('answer', $option['id'])->get()->count();
                        if ($option['default_option_id'] == 1 || $option['default_option_id'] == 2)
                            $sumOfGood += $opt_rep;
                        if ($option['default_option_id'] == 3 || $option['default_option_id'] == 4)
                            $sumOfBad += $opt_rep;
                        if ($option['default_option_id'] == 5)
                            $sumOfYes += $opt_rep;
                        if ($option['default_option_id'] == 6)
                            $sumOfNo += $opt_rep;
                        $percent = ($opt_rep * 100) / $opt_repAll;
                        $questions_array[$key]['options'][$key2]['stat'] = round($percent, 2);

                        if ($options[count($options) - 1] == $option) {
                            $data .= '"' . $option['titre_option'] . '"]';
                            $per .= '"' . round($percent, 2) . '"]';
                        } else {
                            $data .= '"' . $option['titre_option'] . '",';
                            $per .= '"' . round($percent, 2) . '",';
                        }
                        $questions_array[$key]['data'] = $data;
                        $questions_array[$key]['per'] = $per;

                        //dd($percent);
                    }
                    //dd($sumOfGood);

                }

            }
            if (in_array($question['type_reponse_id'], [9,10])) {
                //dd($options);
                $data = '[';
                $per = '[';
                $questions_array[$key]['options'] = $options;
                $opt_repAll = 0;
                foreach($options as $opt){
                    //dd($opt);
                    $opt_repAll += Reponse::where([['question_id', $question['id']],['answer','LIKE','%'.$opt['id'].'%']])->get()->count();
                }
                if ($opt_repAll > 0) {

                    foreach ($options as $key2 => $option) {
                        $opt_rep = Reponse::where([['question_id', $question['id']],['answer','LIKE','%'.$option['id'].'%']])->get()->count();
                        $percent = ($opt_rep * 100) / $opt_repAll;
                        $questions_array[$key]['options'][$key2]['stat'] = round($percent, 2);

                        if ($options[count($options) - 1] == $option) {
                            $data .= '"' . $option['titre_option'] . '"]';
                            $per .= '"' . round($percent, 2) . '"]';
                        } else {
                            $data .= '"' . $option['titre_option'] . '",';
                            $per .= '"' . round($percent, 2) . '",';
                        }
                        $questions_array[$key]['data'] = $data;
                        $questions_array[$key]['per'] = $per;

                        //dd($percent);
                    }
                    //dd($sumOfGood);

                }

            }
            if (in_array($question['type_reponse_id'], [5, 6, 7])) {
                $quest_rep = Reponse::select('answer')->where('question_id', $question['id'])->get()->toArray();
                $questions_array[$key]['reponses'] = $quest_rep;

            }

        }


        $globalStat = ["smile" => ["happy" => $sumOfGood, "sad" => $sumOfBad], "trueFalse" => ["yes" => $sumOfYes, "no" => $sumOfNo]];
        // add something
        //dd($sumOfNo);
        return view('back.sondage.show', compact('sondage', 'questions', 'questions_array', 'nbr_reponse', 'globalStat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sondage $sondage
     * @return \Illuminate\Http\Response
     */
    public
    function edit(Sondage $sondage)
    {


        return view('back.sondage.edit', compact('sondage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Sondage $sondage
     * @return \Illuminate\Http\Response
     */
    public
    function update(SondageRequest $request, Sondage $sondage)
    {
        $imageName = "";
        if(isset($request->logo) != ""){
            $imageName = time() . '.' . request()->logo->getClientOriginalExtension();
            request()->logo->move(public_path('images/admin/sondages'), $imageName);
        }

        $sondage->update([
            'titre' => $request->titre,
            'couleur' => $request->couleur,
            'logo' => $imageName,
            'start_text' => $request->start_text,
            'end_text' => $request->end_text,
        ]);

        return redirect()->route('sondages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sondage $sondage
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(Sondage $sondage)
    {

        $groupes = GroupeDevice::where('sondage_id', $sondage->id)->get();


        foreach ($groupes as $groupe) {
            Device::where('groupe_device_id', $groupe->id)->delete();
        }

        GroupeDevice::where('sondage_id', $sondage->id)->delete();
        $sondage->delete();

        return redirect()->route('sondages.index');
    }




    public function statView(Sondage $sondage){
        if($sondage->user_id != Auth::user()->id)
            abort(404);
        $match_close = ['sondage_id' => $sondage->id, 'is_sousquestion' => 0];
        $questions = Question::where($match_close)->get();
        $questions_array = $questions->toArray();
        $nbr_reponse = Sondage::findOrFail($sondage->id)->reponses->count();
        $sumOfYes = 0;
        $sumOfNo = 0;
        $sumOfGood = 0;
        $sumOfBad = 0;


        foreach ($questions_array as $key => $question) {
            //dd($question);
            $options = Option::where('question_id', $question['id'])->get()->toArray();
            $opt_repAll = Reponse::where('question_id', $question['id'])->get()->count();

            if ($question['type_reponse_id'] == 8) {
                $participant_ids = Reponse::select('participant_id')->where('sondage_id', $question['sondage_id'])->distinct('participant_id')->get()->toArray();
                $participant = array();
                foreach ($participant_ids as $participant_id) {
                    $part = Participant::find($participant_id)->toArray();
                    if ($part != null) {
                        $participant[] = $part;
                    }
                }
                $questions_array[$key]['participants'] = $participant;
                //dd($participant);
            }
            if (in_array($question['type_reponse_id'], [1, 2, 3, 4])) {
                //dd($options);
                $options = Option::where('question_id', $question['id'])->get()->toArray();
                $data = '[';
                $per = '[';
                $questions_array[$key]['options'] = $options;
                if ($opt_repAll > 0) {
                    foreach ($options as $key2 => $option) {
                        $opt_rep = Reponse::where('answer', $option['id'])->get()->count();
                        if ($option['default_option_id'] == 1 || $option['default_option_id'] == 2)
                            $sumOfGood += $opt_rep;
                        if ($option['default_option_id'] == 3 || $option['default_option_id'] == 4)
                            $sumOfBad += $opt_rep;
                        if ($option['default_option_id'] == 5)
                            $sumOfYes += $opt_rep;
                        if ($option['default_option_id'] == 6)
                            $sumOfNo += $opt_rep;
                        $percent = ($opt_rep * 100) / $opt_repAll;
                        $questions_array[$key]['options'][$key2]['stat'] = round($percent, 2);

                        if ($options[count($options) - 1] == $option) {
                            $data .= '"' . $option['titre_option'] . '"]';
                            $per .= '"' . round($percent, 2) . '"]';
                        } else {
                            $data .= '"' . $option['titre_option'] . '",';
                            $per .= '"' . round($percent, 2) . '",';
                        }
                        $questions_array[$key]['data'] = $data;
                        $questions_array[$key]['per'] = $per;

                        //dd($percent);
                    }
                    //dd($sumOfGood);

                }

            }
            if (in_array($question['type_reponse_id'], [9,10])) {
                //dd($options);
                $data = '[';
                $per = '[';
                $questions_array[$key]['options'] = $options;
                $opt_repAll = 0;
                foreach($options as $opt){
                    //dd($opt);
                    $opt_repAll += Reponse::where([['question_id', $question['id']],['answer','LIKE','%'.$opt['id'].'%']])->get()->count();
                }
                if ($opt_repAll > 0) {

                    foreach ($options as $key2 => $option) {
                        $opt_rep = Reponse::where([['question_id', $question['id']],['answer','LIKE','%'.$option['id'].'%']])->get()->count();
                        $percent = ($opt_rep * 100) / $opt_repAll;
                        $questions_array[$key]['options'][$key2]['stat'] = round($percent, 2);

                        if ($options[count($options) - 1] == $option) {
                            $data .= '"' . $option['titre_option'] . '"]';
                            $per .= '"' . round($percent, 2) . '"]';
                        } else {
                            $data .= '"' . $option['titre_option'] . '",';
                            $per .= '"' . round($percent, 2) . '",';
                        }
                        $questions_array[$key]['data'] = $data;
                        $questions_array[$key]['per'] = $per;

                        //dd($percent);
                    }
                    //dd($sumOfGood);

                }

            }
            if (in_array($question['type_reponse_id'], [5, 6, 7])) {
                $quest_rep = Reponse::select('answer')->where('question_id', $question['id'])->get()->toArray();
                $questions_array[$key]['reponses'] = $quest_rep;

            }

        }


        $globalStat = ["smile" => ["happy" => $sumOfGood, "sad" => $sumOfBad], "trueFalse" => ["yes" => $sumOfYes, "no" => $sumOfNo]];
        $statData=["sondage" => $sondage, "questions" => $questions, "questions_array" => $questions_array, "globalStat" => $globalStat, "nbr_reponse" => $nbr_reponse];

        // add something
        //dd($sumOfNo);
        return view('back.sondage.overview', compact('sondage', 'questions', 'questions_array', 'nbr_reponse', 'globalStat','statData'));
    }
}
