<?php

namespace App\Http\Controllers\Admin;

use App\Device;
use App\GroupeDevice;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class DeviceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function viewDevice($id){

     $sondages = User::findOrfail(Auth::user()->id)->sondages;
     $mobiles = Device::where('groupe_device_id',$id)->paginate(10);
     $groupe = GroupeDevice::findOrFail($id);


    // dd($mobile);

      return view('back.device.index',compact('mobiles','groupe','sondages'));
    }
}
