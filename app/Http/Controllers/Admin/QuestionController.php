<?php

namespace App\Http\Controllers\Admin;

use App\BibliothequeIcon;
use App\Option;
use App\Question;
use App\Sondage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();


        return view('back.question.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Sondage $sondage)
    {
        return view('back.question.create', compact('sondage'));
    }


    //SousQuestion
    public function createSousQuestion(Option $option)
    {
        $matchese = ["sondage_id" => $option->question->sondage->id, "is_sousquestion" => 1];
        $questions = Question::where($matchese)->get();
        return view('back.sondage.sousquestion.sousquestions', compact('option', 'questions'));
    }

    public function editSousQuestion(Option $option)
    {

        $sousquestion = Option::find($option->id)->questionsMany()->first();


        return view('back.question.editSousQuestion', compact('option', 'sousquestion'));
    }


    public function updatesousQuestion()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function storeSousQuestion(Request $request)
    {
        $option = Option::find($request->option_id);
        $option->has_subquestion = 1;
        $option->save();
        $question = new Question();
        $question->titre_question = $request->question_lbl;
        $question->sondage_id = $request->sondage_id;
        $question->is_sousquestion = 1;
        if(isset($request->mandatory))
            $question->mandatory = $request->mandatory;

        switch ($request->choice) {
            case 'chx1':
                $question->type_reponse_id = 1;
                $options = [['path' => $request->path_img_1, 'id' => 1, 'lbl' => $request->question_smiley_1], ['path' => $request->path_img_2, 'id' => 2, 'lbl' => $request->question_smiley_2], ['path' => $request->path_img_3, 'id' => 3, 'lbl' => $request->question_smiley_3], ['path' => $request->path_img_4, 'id' => 4, 'lbl' => $request->question_smiley_4]];
                break;
            case 'chx2':

                if ($request->lvl2_chx == "lvl2_1") {
                    if(isset($request->multi_choice)){
                        $question->type_reponse_id = 9;
                    }else{
                        $question->type_reponse_id = 2;
                    }
                    $options = array();
                    foreach ($request->qcmtext as $input) {
                        $options[] = ['path' => 'no_path', 'id' => 0, 'lbl' => $input];
                    }
                } else {
                    if(isset($request->multi_choice)){
                        $question->type_reponse_id = 10;
                    }else{
                        $question->type_reponse_id = 3;
                    }
                    for ($i = 0; $i < count($request->personnal_img); $i++) {
                        $imageName = dechex(mt_rand(1000000000, 9999999999)) . time() . 'optimg.' . $request->personnal_img[$i]->getClientOriginalExtension();
                        $request->personnal_img[$i]->move(public_path('images/admin/sondages/userImg/optionImg'), $imageName);
                        $options[] = ['path' => 'images/admin/sondages/userImg/optionImg/' . $imageName, 'id' => 0, 'lbl' => $request->img_input[$i]];
                    }

                }
                break;
                // add something
            case 'chx3':
                $question->type_reponse_id = 4;
                $options = [['path' => '✔', 'id' => 5, 'lbl' => $request->yes_input], ['path' => '✘', 'id' => 6, 'lbl' => $request->no_input]];
                break;
            case 'chx4':
                $question->type_reponse_id = 5;
                $question->save();
                $last = Question::orderBy('id', 'DESC')->first();
                $option->questionsMany()->attach($last->id);
                return redirect()->back();
                break;
            case 'chx5':
                $question->type_reponse_id = 6;
                $question->save();
                $last = Question::orderBy('id', 'DESC')->first();
                $option->questionsMany()->attach($last->id);
                return redirect()->back();
                break;
            case 'chx6':
                $question->type_reponse_id = 7;
                $question->save();
                $last = Question::orderBy('id', 'DESC')->first();
                $option->questionsMany()->attach($last->id);
                $question->save();
                return redirect()->back();
                break;
            default:
                dd('default case');
                break;
        }
        $question->save();
        $last = Question::orderBy('id', 'DESC')->first();
        $option->questionsMany()->attach($last->id);
        foreach ($options as $opt) {
            $option = new Option();
            $option->titre_option = $opt['lbl'];
            $option->has_subquestion = 0;
            $option->default_option_id = $opt['id'];
            $option->path_img = $opt['path'];
            $option->question_id = Question::orderBy('id', 'DESC')->first()->id;
            $option->save();
        }

        return redirect()->back();
    }


    public function store(Request $request)
    {
        //dd($request);
        $question = new Question();
        $question->titre_question = $request->question_lbl;
        $question->sondage_id = $request->sondage_id;
        $question->is_sousquestion = 0;
        if(isset($request->mandatory))
            $question->mandatory = $request->mandatory;
        switch ($request->choice) {
            case 'chx1':
                $question->type_reponse_id = 1;
                $options = [['path' => $request->path_img_1, 'id' => 1, 'lbl' => $request->question_smiley_1], ['path' => $request->path_img_2, 'id' => 2, 'lbl' => $request->question_smiley_2], ['path' => $request->path_img_3, 'id' => 3, 'lbl' => $request->question_smiley_3], ['path' => $request->path_img_4, 'id' => 4, 'lbl' => $request->question_smiley_4]];
                break;
            case 'chx2':

                if ($request->lvl2_chx == "lvl2_1") {
                    if(isset($request->multi_choice)){
                        $question->type_reponse_id = 9;
                    }else{
                        $question->type_reponse_id = 2;
                    }
                    $options = array();
                    foreach ($request->qcmtext as $input) {
                        $options[] = ['path' => 'no_path', 'id' => 0, 'lbl' => $input];

                    }
                } else {
                    if(isset($request->multi_choice)){
                        $question->type_reponse_id = 10;
                    }else{
                        $question->type_reponse_id = 3;
                    }
                    for ($i = 0; $i < count($request->personnal_img); $i++) {
                        $imageName = dechex(mt_rand(1000000000, 9999999999)).time() . 'optimg.' . $request->personnal_img[$i]->getClientOriginalExtension();
                        $request->personnal_img[$i]->move(public_path('images/admin/sondages/userImg/optionImg'), $imageName);
                        $options[] = ['path' => 'images/admin/sondages/userImg/optionImg/' . $imageName, 'id' => 0, 'lbl' => $request->img_input[$i]];
                    }

                }
                break;
            case 'chx3':
                $question->type_reponse_id = 4;
                $options = [['path' => '✔', 'id' => 5, 'lbl' => $request->yes_input], ['path' => '✘', 'id' => 6, 'lbl' => $request->no_input]];
                break;
            case 'chx4':
                $question->type_reponse_id = 5;
                $question->save();
                return redirect()->back();
                break;
            case 'chx5':
                $question->type_reponse_id = 6;
                $question->save();
                return redirect()->back();
                break;
            case 'chx6':
                $question->type_reponse_id = 7;
                $question->save();
                return redirect()->back();
                break;
            default:
                dd('default case');
                break;
        }
        $question->save();
        foreach ($options as $opt) {
            $option = new Option();
            $option->titre_option = $opt['lbl'];
            $option->has_subquestion = 0;
            $option->default_option_id = $opt['id'];
            $option->path_img = $opt['path'];
            $option->question_id = Question::orderBy('id', 'DESC')->first()->id;
            $option->save();
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        return view('back.question.edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Question $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        $question->delete();
        return redirect()->route('questions.index');
    }
}
