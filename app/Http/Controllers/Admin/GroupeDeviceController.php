<?php

namespace App\Http\Controllers\Admin;

use App\GroupeDevice;
use App\Option;
use App\Participant;
use App\Question;
use App\Reponse;
use App\Sondage;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;


class GroupeDeviceController extends Controller
{


    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $groupes = User::findOrFail(Auth::user()->id)->groupes()->paginate(3);


        $sondages = User::findOrfail(Auth::user()->id)->sondages->where('mobile','=',1);




       return view('back.groupe.index',compact('groupes','sondages'));
    }

    public function groupesStat(GroupeDevice $groupeDevice)
    {
        $sondage = GroupeDevice::find($groupeDevice->id)->sondage;
        $match_close = ['sondage_id' => $sondage->id, 'is_sousquestion' => 0];
        $questions = Question::where($match_close)->get();
        $questions_array = $questions->toArray();
        $nbr_reponse = Sondage::findOrFail($sondage->id)->reponses->count();
        $sumOfYes = 0;
        $sumOfNo = 0;
        $sumOfGood = 0;
        $sumOfBad = 0;
        $reponse_in_grpe = Reponse::where(['group_id' => $groupeDevice->id,'sondage_id' => $sondage->id]);
        $nbr_reponse = $reponse_in_grpe->count();
        if ($reponse_in_grpe->count()>0){
            foreach ($questions_array as $key => $question) {
                $options = Option::where('question_id', $question['id'])->get()->toArray();
                $opt_repAll = Reponse::where('question_id', $question['id'])->get()->count();
                if ($question['type_reponse_id'] == 8) {
                    $participant_ids = Reponse::select('participant_id')->where('sondage_id', $question['sondage_id'])->distinct('participant_id')->get()->toArray();
                    $participant = array();
                    foreach ($participant_ids as $participant_id) {
                        $part = Participant::find($participant_id)->toArray();
                        if ($part != null) {
                            $participant[] = $part;
                        }
                    }
                    $questions_array[$key]['participants'] = $participant;
                    //dd($participant);
                }
                if (in_array($question['type_reponse_id'], [1, 2, 3, 4])) {
                    //dd($options);
                    $data = '[';
                    $per = '[';
                    $questions_array[$key]['options'] = $options;
                    if ($opt_repAll > 0) {
                        foreach ($options as $key2 => $option) {
                            $opt_rep = Reponse::where(['answer' => $option['id'],'group_id' => $groupeDevice->id])->get()->count();
                            if ($option['default_option_id'] == 1 || $option['default_option_id'] == 2)
                                $sumOfGood += $opt_rep;
                            if ($option['default_option_id'] == 3 || $option['default_option_id'] == 4)
                                $sumOfBad += $opt_rep;
                            if ($option['default_option_id'] == 5)
                                $sumOfYes += $opt_rep;
                            if ($option['default_option_id'] == 6)
                                $sumOfNo += $opt_rep;
                            $percent = ($opt_rep * 100) / $opt_repAll;
                            $questions_array[$key]['options'][$key2]['stat'] = round($percent, 2);

                            if ($options[count($options) - 1] == $option) {
                                $data .= '"' . $option['titre_option'] . '"]';
                                $per .= '"' . round($percent, 2) . '"]';
                            } else {
                                $data .= '"' . $option['titre_option'] . '",';
                                $per .= '"' . round($percent, 2) . '",';
                            }
                            $questions_array[$key]['data'] = $data;
                            $questions_array[$key]['per'] = $per;

                            //dd($percent);
                        }
                        //dd($sumOfGood);

                    }

                }
                if (in_array($question['type_reponse_id'], [5, 6, 7])) {
                    $quest_rep = Reponse::select('answer')->where(['question_id' => $question['id'],'group_id' => $groupeDevice->id])->get()->toArray();
                    $questions_array[$key]['reponses'] = $quest_rep;

                }

            }
        }


        $globalStat = ["smile" => ["happy" => $sumOfGood, "sad" => $sumOfBad], "trueFalse" => ["yes" => $sumOfYes, "no" => $sumOfNo]];
        // add something
        //dd($sumOfNo);
        return view('back.groupe.stat', compact('sondage', 'questions', 'questions_array', 'nbr_reponse', 'globalStat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sondages = User::findOrfail(Auth::user()->id)->sondages;

        return view('back.groupe.create',compact('sondages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        request()->validate([
            'titre'=> 'required',
            'sondage'=> 'required'
        ]);


/*
        if(User::findOrFail(Auth::user()->id)->groupes()->where('libelle','=',$request->titre)){

            $error = "un groupe porte deja le titre ".$request->titre;
            return redirect()->route('groupesdevices.index')->with('error',$error);
        }*/


        $groupe = new GroupeDevice();
        $groupe->libelle = $request->titre;
        $groupe->sondage_id = $request->sondage;
        $groupe->user_id = Auth::user()->id;
        $groupe->save();

        return redirect()->route('groupesdevices.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GroupeDevice  $groupeDevice
     * @return \Illuminate\Http\Response
     */
    public function show(GroupeDevice $groupeDevice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GroupeDevice  $groupeDevice
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sondages = User::findOrfail(Auth::user()->id)->sondages;
        $groupe = GroupeDevice::findOrFail($id);
         //dd($groupe->sondage_id);
        return view('back.groupe.edit',compact('groupe','sondages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GroupeDevice  $groupeDevice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       // dd($request->sondage);
      $groupeDevice = GroupeDevice::findOrFail($id);

      $groupeDevice->libelle = $request->titre;
      $groupeDevice->sondage_id =$request->sondage;
      $groupeDevice->save();

        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GroupeDevice  $groupeDevice
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $groupeDevice = GroupeDevice::findOrFail($id);
        $groupeDevice->delete();
        return redirect()->route('groupesdevices.index');
    }
}
