<?php

namespace App\Http\Controllers\Admin;

use App\Sondage;
use App\User;
use App\Question;
use App\Option;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class WebSondageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sondages = User::findOrFail(Auth::user()->id)->sondages->where('web','=',1);

        return view('back.web.index',compact('sondages'));
    }



    public function statistiqueWeb($id){

        $sondage  = Sondage::findOrFail($id);
        return view('back.web.statistique',compact('sondage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function viewSondage($id){
        $sondage  = Sondage::findOrFail($id)->toArray();
        $questions = Sondage::findOrfail($sondage['id'])->questions->toArray();

        $sondage['questions'] = $questions;

        foreach ($questions as $key =>$question){
            $options = Question::findOrfail($question['id'])->options->toArray();
            $sondage["questions"][$key]["options"] = $options;

            foreach ($options as $key2 => $option){

                $sousquestion= Option::findOrfail($option['id'])->questionsMany->first();

                $sondage["questions"][$key]["options"][$key2]["sondageid"] =  $sondage["questions"][$key]['sondage_id'];
                $sondage["questions"][$key]["options"][$key2]["sousquestions"] = $sousquestion["id"];
            }

        }
        $sondage = json_encode($sondage);
        return view('front.sondage',compact('sondage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
