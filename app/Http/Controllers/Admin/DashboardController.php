<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use App\Sondage;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function viewDashboard(){
        $countS = Sondage::where('user_id', Auth::user()->id)->get()->count();
        return view('back.dashboard', compact('countS'));
    }
}
