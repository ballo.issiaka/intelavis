<?php

namespace App\Http\Controllers\Admin;

use App\EmailParticipant;
use App\Sondage;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Mail\LinkMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class EmailParticipantController extends Controller
{


    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $email = User::findOrfail(Auth::user()->id)->emails;
        $sondages = User::findOrFail(Auth::user()->id)->sondages->where('email','=',1);

        return view('back.customermail.index',compact('sondages'));
    }




    public function statistiqueEmail($id){

        $sondage  = Sondage::findOrFail($id);
        return view('back.customermail.statistique',compact('sondage'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.customermail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $emailcustomer = new EmailParticipant();
        $emailcustomer->email = $request->email;
        $emailcustomer->user_id = Auth::user()->id;
        $emailcustomer->save();

        return redirect()->route('customeremails.index ');
    }



   public function createForm($id){

        $sondage = Sondage::findOrFail($id);

        return view('back.customermail.create',compact('sondage'));

   }


    public function sendMail(Request $request){

        $emails = $request->listemails;
        $sondageid = $request->sondageid;
        $message = $request->message;

        $sondage = Sondage::findOrFail($sondageid);
        $sondagelink = $sondage->getUrlAttribute();



        foreach ($emails as $email){

            $emailcustomer = EmailParticipant::updateOrCreate(
                [
                  'email'=>$email,
                    'user_id'=>Auth::user()->id
                ]
            );

            $token = str_random(12);
            $link =$sondagelink."/".$token;
            $emailcustomer->sondages()->attach($sondageid,[
                'token'=>$token,
                'is_use'=>0,
                'message'=>$message
            ]);
            Mail::to($email)->send(new LinkMail($message,$link));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function sondageByMail($id){

        $datas = DB::table('email_participant_sondage')
            ->leftJoin('sondages','sondages.id','=','email_participant_sondage.sondage_id')
            ->leftJoin('email_participants','email_participants.id','=','email_participant_sondage.email_participant_id')
            ->where('email_participant_sondage.sondage_id',$id)
            ->select('sondages.*','email_participant_sondage.*','email_participants.*')
            ->paginate(10);


        return view('back.customermail.sondagebymail',compact('datas'));




    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
