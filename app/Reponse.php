<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reponse extends Model
{

    protected $fillable = ['answer','plateforme','group_id','question_id','participant_id','sondage_id'];

    public function question(){

        return $this->belongsTo(Question::class);
    }

    public function participant(){

        return $this->belongsTo(Participant::class);
    }

    public  function sondage(){
        return $this->belongsTo(Sondage::class);
    }

}
