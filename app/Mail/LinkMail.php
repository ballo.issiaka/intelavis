<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LinkMail extends Mailable
{
    use Queueable, SerializesModels;

     public $mesazh;
     public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($msg,$lien)
    {
        $this->mesazh = $msg;
        $this->link = $lien;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('back.sendemail.index');
    }
}
