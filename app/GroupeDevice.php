<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupeDevice extends Model
{
    protected $fillable = ['libelle'];

    public function sondage(){
        return $this->belongsTo(Sondage::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function devices(){
        return $this->hasMany(Device::class);
    }
}
