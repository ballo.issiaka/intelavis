<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abonnement extends Model
{
    protected $fillable = ['libelle','max_sondages','max_plateformes','max_questions','max_reponses','max_devices'];


    public function users(){

        return $this->hasMany(User::class);
    }
}
