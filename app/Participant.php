<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{

    protected $fillable = ['nom','email','numero'];
    public function reponses(){

        return $this->hasMany(Reponse::class);
    }
}
