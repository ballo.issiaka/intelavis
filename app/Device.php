<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{

    protected $fillable = ['model','plateforme','groupe_device_id'];

    public function groupe(){

        return $this->belongsTo(GroupeDevice::class);
    }
}
