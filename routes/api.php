<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Sondage
Route::resource('sondages','Api\SondageController',['only'=>['index','show']]);

//Device
Route::resource('devices','Api\DeviceController');

//Reponse
Route::resource('reponses','Api\ReponseController');

//Client
Route::post('mobile/login','Api\MobileController@loginMobile')->name('login.mobile');
Route::get('mobile/getsondage/{groupeid}','Api\MobileController@sondageGroupe');
Route::get('mobile/getuser/{useremail}','Api\MobileController@getUser');
Route::get('mobile/groupes/{userid}','Api\MobileController@indexGroupe');
Route::post('mobile/device','Api\MobileController@storeGetDevice');
Route::get('mobile/device','Api\MobileController@storeGetDevice');
Route::get('mobile/device/delete/{deviceid}','Api\MobileController@deleteDevice');
Route::post('mobile/device/sendanswer','Api\MobileController@storeReponse');

//webClient

Route::post('web/saverep/','Api\WebController@storeReponse');
Route::get('sondage/stat/{id}','Api\SondageController@statView');

