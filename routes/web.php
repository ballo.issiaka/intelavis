<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Admin\DashboardController@viewDashboard')->name('dashboard.show');

Route::get('/dashboard','Admin\DashboardController@viewDashboard')->name('dashboard.show');
Route::get('/survey','Admin\SurveyController@viewSurvey');

//Sondage
Route::resource('sondages','Admin\SondageController');
Route::get('sondages/link/{slug}','Admin\SondageController@linkSondage')->name('link.sondage');
Route::get('sondages/stat/{sondage}','Admin\SondageController@statView')->name('stat.sondage');

//User
Route::resource('users','Admin\UserController')->except(['index']);


//Question
Route::resource('questions','Admin\QuestionController')->except(['create']);
Route::get('/questions/{sondage}/create', 'Admin\QuestionController@create')->name('question.create');

//Sous Question
Route::get('/sousquestions/{option}/create','Admin\QuestionController@createSousQuestion')->name('sousquestion.create');
Route::post('/sousquestions/store','Admin\QuestionController@storeSousQuestion')->name('sousquestion.store');
Route::get('/sousquestions/{option}/edit','Admin\QuestionController@editSousQuestion')->name('sousquestion.edit');


//GroupeDevice
Route::resource('groupesdevices','Admin\GroupeDeviceController');
Route::get('groupesdevices/stat/{groupeDevice}','Admin\GroupeDeviceController@groupesStat')->name('devices.stat');

//Devices
Route::get('/devices/{groupe}','Admin\DeviceController@viewDevice')->name('devices.view');

//Email
Route::resource('customeremails','Admin\EmailParticipantController');
Route::get('customeremail/{sondage}','Admin\EmailParticipantController@createForm')->name('customeremail.form');
Route::get('customeremail/{sondage}/mails','Admin\EmailParticipantController@sondageByMail')->name('mails.sondage');
Route::post('customeremail/store','Admin\EmailParticipantController@sendMail')->name('mails.store');
Route::get('web/sondagemail/{id}','Admin\EmailParticipantController@statistiqueEmail')->name('email.sondagemail');


//Web
Route::resource('web','Admin\WebSondageController');
Route::get('web/sondageweb/{id}','Admin\WebSondageController@statistiqueWeb')->name('web.sondageweb');
Route::get('web/sondage/{id}','Admin\WebSondageController@viewSondage')->name('web.viewsondage');



//Envoi de mail
Route::get('/sendmails','Admin\EmailParticipantController@sendmail');

//




Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

